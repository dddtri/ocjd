package suncertify.db;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class CustomUncaughtExceptionHandler implements UncaughtExceptionHandler {
	
	private Map<Thread, Throwable> errors;
	public CustomUncaughtExceptionHandler() {
		this.resetErrors();
	}

	public void uncaughtException(Thread t, Throwable e) {
		errors.put(t, e);
	}

	public Map<Thread, Throwable> getErrors() {
		return errors;
	}

	public void resetErrors() {
		this.errors = new LinkedHashMap<Thread, Throwable>();
	}

	public void throwError() {
		StringBuilder sb = new StringBuilder();
		if (this.errors.size() > 0) {
			sb.append("count:" + this.errors.size() + "\n");	
		}
		for (Entry<Thread, Throwable> entry : this.errors.entrySet()) {
			sb.append("\n");
			sb.append(entry.getKey().getName() + ":\n");
			sb.append(entry.getValue().getMessage() + "\n");
			sb.append(entry.getValue().getClass().getName() + "\n");
			for (StackTraceElement trace : entry.getValue().getStackTrace()) {
				sb.append(trace + "\n");
			}
		}
		if (sb.length() > 0) {
			throw new IllegalStateException(sb.toString());
		}
	}
	
}

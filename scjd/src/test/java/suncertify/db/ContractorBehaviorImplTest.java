package suncertify.db;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import suncertify.Mode;
import suncertify.gui.RecordsTableModel;
import suncertify.service.ContractorBehavior;
import suncertify.service.impl.ContractorBehaviorImpl;

public class ContractorBehaviorImplTest {

	@Test
	public void testOrSearchOnNameSearchTerm() throws IOException {
		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);


		ContractorBehavior cb = new ContractorBehaviorImpl(Mode.ALONE);

		Assert.assertEquals(0, cb.searchRecords(false, true,
				RecordsTableModel.getFormattedLocationString(""),
				RecordsTableModel.getFormattedNameString("Moore")).length);

		Assert.assertEquals(5, cb.searchRecords(false, true,
				RecordsTableModel.getFormattedLocationString("abcdef"),
				RecordsTableModel.getFormattedNameString("Moore Power Tool Ya")).length);
	}
	
	
	@Test
	public void testOrSearchOnOwnerSearchTerm() throws IOException {
		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);


		ContractorBehavior cb = new ContractorBehaviorImpl(Mode.ALONE);

		Assert.assertEquals(0, cb.searchRecords(false, true,
				RecordsTableModel.getFormattedLocationString(""),
				RecordsTableModel.getFormattedNameString("LendMarch")).length);

		Assert.assertEquals(7, cb.searchRecords(false, true,
				RecordsTableModel.getFormattedLocationString("Lendmarch"),
				RecordsTableModel.getFormattedNameString("Moore Power Tool Ya")).length);
	}

	@Test
	public void testDefaultSearchTerm() throws IOException {
		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);


		ContractorBehavior cb = new ContractorBehaviorImpl(Mode.ALONE);

		Assert.assertEquals(29, cb.searchRecords(false, true,
				null,
				null).length);

		Assert.assertEquals(29, cb.searchRecords(true, false,
				null,
				null).length);
		
		
		Assert.assertEquals(29, cb.searchRecords(true, false,
				"",
				"").length);
		
		Assert.assertEquals(29, cb.searchRecords(true, false,
				null,
				"").length);
	}

}

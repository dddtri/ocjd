package suncertify.db;

import java.io.File;
import java.io.IOException;
import java.util.Random;
public class UpdateRecordThread extends Thread {

	private int recNum;
	public UpdateRecordThread(int recNum) {
		this.recNum = recNum;
	}
	
	@Override
	public void run() {
		DBAccess data = null;
		try {
			File file = new File("db-2x1-test.db");
			data = new Data(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}			
			try {
				
				long lockCookie = data.lockRecord(this.recNum);
				data.updateRecord(
						this.recNum,
						new String[]{
						"name_" + DataTest.generateString(new Random(), "abcdefghijklmnopqrstuvwxyz", 27),
						"city_" + DataTest.generateString(new Random(), "abcdefghijklmnopqrstuvwxyz", 59),
						"typeofwork_" + DataTest.generateString(new Random(), "abcdefghijklmnopqrstuvwxyz", 53),
						DataTest.generateString(new Random(), "1234567890", 6),
						"$" + DataTest.generateString(new Random(), "1234567890", 7),
						DataTest.generateString(new Random(), "1234567890", 8)
						}, 
						lockCookie);
				data.unlock(recNum, lockCookie);
			} catch (SecurityException e) {
				throw new AssertionError(e);
			} catch (RecordNotFoundException e) {
				throw new AssertionError(e);
			}
	}

	
}

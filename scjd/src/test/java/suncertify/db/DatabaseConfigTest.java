package suncertify.db;

import java.util.Properties;

import junit.framework.Assert;

import org.junit.Test;

import suncertify.DatabaseConfig;

public class DatabaseConfigTest {

	@Test
	public void test() {
		DatabaseConfig dbCfg = new DatabaseConfig();
		Properties properties = dbCfg.load();
		properties.setProperty(DatabaseConfig.PRPTY_NAME_MODE_STANDALONE_DB_FILE_PATH, "C:\\Users\\daniel\\git\\ocjd\\scjd\\db-2x1-test.db");
		Assert.assertTrue(dbCfg.store());
	}
}

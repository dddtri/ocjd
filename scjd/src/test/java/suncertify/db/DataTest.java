package suncertify.db;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DataTest {

	static private CustomUncaughtExceptionHandler expHandler;

	@BeforeClass
	static public void beforeClass() {
		Logger.getLogger("").getHandlers()[0].setLevel(Level.ALL);
		Logger.getLogger("").setLevel(Level.ALL);
		expHandler = new CustomUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(expHandler);		
	}

	@Before
	public void before() throws IOException {
		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);

		//reload db in memory
		new Data(destFile);
		Data.getDataFileHander().load();
		expHandler.resetErrors();
	}

	@Test
	public void testCreateRecordThreads() throws IOException {
		List<Thread> ts = new LinkedList<Thread>();

		for (int i = 0 ; i < 200 ; i ++) {
			ts.add(new CreateRecordThread());
		}

		ClientRunner client = new MockClientRunner();
		client.run(ts);
		expHandler.throwError();

	}


	@Test
	public void testUpdateRecordThreads() throws IOException {
		List<Thread> ts = new LinkedList<Thread>();
		//int recNum = 1;
		int recNum = (int) (Math.random() * 1000000000 % Data.getDataFileHander().getRecords().size());
		for (int i = 0 ; i < 1000 ; i ++) {
			ts.add(new UpdateRecordThread(recNum));
		}

		ClientRunner client = new MockClientRunner();
		client.run(ts);
		expHandler.throwError();

	}

	@Test
	public void testDeleteRecordThreads() throws IOException {
		List<Thread> ts = new LinkedList<Thread>();

		for (int i = 0 ; i < 28 ; i ++) {
			ts.add(new DeleteRecordThread());
		}

		ClientRunner client = new MockClientRunner();
		client.run(ts);
		expHandler.throwError();

	}


	@Test
	public void testReadRecordThreads() throws IOException {
		List<Thread> ts = new LinkedList<Thread>();
		int recNum = 1;
		for (int i = 0 ; i < 1000 ; i ++) {
			ts.add(new ReadRecordThread(recNum));
		}

		ClientRunner client = new MockClientRunner();
		client.run(ts);
		expHandler.throwError();

	}


	@Test
	public void testMix() throws IOException {
		List<Thread> ts = new LinkedList<Thread>();
		int recNum = 1;
		for (int i = 0 ; i < 10 ; i ++) {
			ts.add(new CreateRecordThread());
			//ts.add(new UpdateRecordThread(recNum));
			ts.add(new ReadRecordThread(recNum));
			ts.add(new FindByCriteriaThread(recNum));
			//ts.add(new DeleteRecordThread());
		}

		ClientRunner client = new MockClientRunner();
		client.run(ts);
		expHandler.throwError();
	}


	@Test
	public void testUpdateAndFind() throws IOException {
		List<Thread> ts = new LinkedList<Thread>();
		int recNum = 0;
		for (int i = 0 ; i < 1000; i ++) {
			ts.add(new UpdateRecordThread(recNum));
			ts.add(new FindByCriteriaThread(recNum));
			//ts.add(new DeleteRecordThread());
		}

		ClientRunner client = new MockClientRunner();
		try {
			client.run(ts);
			expHandler.throwError();
			throw new AssertionError("this test expects to throw assertion error");
		} catch (Exception e) {
			//pass
		}

	}


	@Test
	public void testServiceMix() throws IOException {
		List<Thread> ts = new LinkedList<Thread>();
		//int recNum = (int) (Math.random() * 1000000000 % Data.getDataFileHander().getRecords().size());
		int recNum = 1;
		for (int i = 0 ; i < 10 ; i ++) {
			ts.add(new BusinessUpdateRecordThread(recNum));
			ts.add(new BusinessSearchRecordThread(recNum));
			//ts.add(new DeleteRecordThread());
		}

		ClientRunner client = new MockClientRunner();
		client.run(ts);
		expHandler.throwError();
	}


	@Test
	public void testFindByCriteria() throws IOException {
		List<Thread> ts = new LinkedList<Thread>();
		int recNum = 1;
		for (int i = 0 ; i < 1000 ; i ++) {
			ts.add(new FindByCriteriaThread(recNum));
		}

		ClientRunner client = new MockClientRunner();
		client.run(ts);
		expHandler.throwError();
	}

	@Test
	public void testCookie() throws IOException {
		List<Thread> ts = new LinkedList<Thread>();
		for (int i = 0 ; i < 100 ; i ++) {
			long wait = 0;
			ts.add(new CookieThread(wait));
		}

		ClientRunner client = new MockClientRunner();
		client.run(ts);
		expHandler.throwError();
	}	



	public static String generateString(Random rng, String characters, int length)
	{
		char[] text = new char[length];
		for (int i = 0; i < length; i++)
		{
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}

	public static int generateInteger(int min, int max) {

		// Usually this can be a field rather than a method variable
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max + 1) - min) + min;

		return randomNum;
	}

}


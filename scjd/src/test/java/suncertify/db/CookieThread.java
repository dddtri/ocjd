package suncertify.db;

import java.io.File;
import java.io.IOException;

public class CookieThread extends Thread {

	private long waitTime;
	
	public CookieThread(long waitTime) {
		this.waitTime = waitTime;
	}
	
	@Override
	public void run() {
		DBAccess data = null;
		try {
			data = new Data(new File("db-2x1-test.db"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}			
			int recNum = 1;
			long cookieVal;
			try {
				cookieVal = data.lockRecord(recNum);
			} catch (RecordNotFoundException e) {
				throw new IllegalStateException(e);
			}
			try {
				Thread.sleep(this.waitTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			data.unlock(recNum, cookieVal);
	}

}

package suncertify.db;

import java.io.IOException;
import java.util.List;

import suncertify.Mode;
import suncertify.gui.RecordsTableModel;
import suncertify.service.ContractorBehavior;
import suncertify.service.impl.ContractorBehaviorImpl;

public class BusinessUpdateRecordThread extends Thread {
	
	private int recNum;
	public BusinessUpdateRecordThread(int recNum) {
		this.recNum = recNum;
	}

	@Override
	public void run() {
		ContractorBehavior cb = null;
		try {
			cb = new ContractorBehaviorImpl(Mode.ALONE);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		DataFileHandler  data = Data.getDataFileHander();
		List<List<String>> records = data.getRecords();
		//int idx = (int) (Math.random() * 1000000000 % records.size());
		List<String> record = records.get(recNum);
		try {
			cb.updateRecord(new String[]{
					record.get(1),
					record.get(2),
					record.get(3),
					record.get(4),
					record.get(5),
					RecordsTableModel.getFormattedOwnerString(String.valueOf(recNum))
			});
		} catch (SecurityException e) {
			throw new AssertionError(e);
		}
	}

}

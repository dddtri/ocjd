package suncertify.db;

import java.io.IOException;
import java.util.List;

public class MockClientRunner implements ClientRunner {

	public void run(List<Thread> ts) throws IOException {
		for (int i = 0 ; i < ts.size() ; i ++) {
			ts.get(i).start();
/*			try {
				ts.get(i).join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}*/
		}

		for (int i = 0 ; i < ts.size() ; i ++) {
			try {
				ts.get(i).join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Data.getDataFileHander().printConsole();
	}
}

package suncertify.db;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;
public class FindByCriteriaThread extends Thread {

	private int recNum;
	public FindByCriteriaThread(int recNum) {
		this.recNum = recNum;
	}
	
	
	@Override
	public void run() {
		DBAccess data = null;
		try {
			data = new Data(new File("db-2x1-test.db"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		List<String> record = Data.getDataFileHander().getRecords().get(recNum);
		long rec = data.findByCriteria(
				new String[]{
						record.get(1),
						record.get(2),
						record.get(3),
						record.get(4),
						record.get(5),
						record.get(6)
				})[0];
	}


}

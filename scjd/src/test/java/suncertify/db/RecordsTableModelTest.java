package suncertify.db;

import junit.framework.Assert;

import org.junit.Test;

import suncertify.gui.RecordsTableModel;

public class RecordsTableModelTest {

	@Test(expected = NullPointerException.class)
	public void testGetFormattedOwnerString_NULL() {
		Assert.assertEquals(8, RecordsTableModel.getFormattedOwnerString(null).length());
	}
	
	@Test
	public void testGetFormattedOwnerString_EmptyString() {
		Assert.assertEquals(8, RecordsTableModel.getFormattedOwnerString("").length());
	}
	
	
	@Test
	public void testGetFormattedOwnerString_SmallString() {
		Assert.assertEquals(8, RecordsTableModel.getFormattedOwnerString("1234").length());
	}
	
	
	@Test
	public void testGetFormattedOwnerString_LargeString() {
		Assert.assertEquals(8, RecordsTableModel.getFormattedOwnerString("1234567890").length());
	}
	
	
	@Test(expected = NullPointerException.class)
	public void testGetFormattedNameString_NULL() {
		Assert.assertEquals(32, RecordsTableModel.getFormattedNameString(null).length());
	}
	
	@Test
	public void testGetFormattedNameString_EmptyString() {
		Assert.assertEquals(32, RecordsTableModel.getFormattedNameString("").length());
	}
	
	
	@Test
	public void testGetFormattedNameString_SmallString() {
		Assert.assertEquals(32, RecordsTableModel.getFormattedNameString("1234").length());
	}
	
	
	@Test
	public void testGetFormattedNameString_LargeString() {
		Assert.assertEquals(32, RecordsTableModel.getFormattedNameString("1234567890").length());
	}
	
	
	
	@Test(expected = NullPointerException.class)
	public void testGetFormattedLocationString_NULL() {
		Assert.assertEquals(64, RecordsTableModel.getFormattedLocationString(null).length());
	}
	
	@Test
	public void testGetFormattedLocationString_EmptyString() {
		Assert.assertEquals(64, RecordsTableModel.getFormattedLocationString("").length());
	}
	
	
	@Test
	public void testGetFormattedLocationString_SmallString() {
		Assert.assertEquals(64, RecordsTableModel.getFormattedLocationString("1234").length());
	}
	
	
	@Test
	public void testGetFormattedLocationString_LargeString() {
		Assert.assertEquals(64, RecordsTableModel.getFormattedLocationString("1234567890").length());
	}
}

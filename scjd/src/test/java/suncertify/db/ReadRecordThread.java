package suncertify.db;

import java.io.File;
import java.io.IOException;

public class ReadRecordThread extends Thread {

	private int recNum;
	public ReadRecordThread(int recNum) {
		this.recNum = recNum;
	}
	
	
	@Override
	public void run() {
		DBAccess data = null;
		try {
			data = new Data(new File("db-2x1-test.db"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}			
		try {
			data.readRecord(this.recNum);
		} catch (SecurityException e) {
			throw new AssertionError(e);
		} catch (RecordNotFoundException e) {
			throw new AssertionError(e);
		}
	}

	
}

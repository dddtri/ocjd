package suncertify.db;

import java.io.IOException;
import java.util.List;

import suncertify.Mode;
import suncertify.service.ContractorBehavior;
import suncertify.service.impl.ContractorBehaviorImpl;

public class BusinessSearchRecordThread extends Thread {
	
	private int recNum;
	public BusinessSearchRecordThread(int recNum) {
		this.recNum = recNum;
	}

	@Override
	public void run() {
		ContractorBehavior cb = null;
		try {
			cb = new ContractorBehaviorImpl(Mode.ALONE);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		boolean isAndSelected = getRandomBoolean();

		DataFileHandler  data = Data.getDataFileHander();
		List<List<String>> records = data.getRecords();
		int idx = recNum;
		List<String> record = records.get(idx);
		try {
			cb.searchRecords(isAndSelected, !isAndSelected,
					record.get(2),
					record.get(1));
		} catch (SecurityException e) {
			throw new AssertionError(e);
		}
		
	}

	public static boolean getRandomBoolean() {
		return Math.random() < 0.5;
		//I tried another approaches here, still the same result

	}
}

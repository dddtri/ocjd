package suncertify.db;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class DataFileHandlerTest {

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Test(expected = FileNotFoundException.class)
	public void testInvaidFile() throws IOException {
		System.out.println(System.getProperty("user.dir"));
		DataFileHandler handler = new DataFileHandler(new File("abcde"));
		handler.printConsole();
	}

	@Test
	public void testPrintConsole() throws IOException {
		DataFileHandler handler = new DataFileHandler(new File("db-2x1.db"));
		handler.printConsole();
	}

	@Test
	public void testWrite() throws IOException {
		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);
		handler.write();
		handler.load();
		handler.printConsole();
	}

	@Test
	public void testUpdateRecord() throws IOException, RecordNotFoundException {

		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);

		String[] record = new String[]{this.getString(32, "subcontractor"), 
				this.getString(64, "city"), 
				this.getString(64, "types of work"), 
				this.getString(6, "size"), 
				this.getString(8, "rate"), 
				this.getString(8, "owner")};

		handler.updateRecord(5, 
				record);
		handler.load();
		handler.printConsole();
		Assert.assertEquals(5, handler.findByCriteria(record)[0]);

	}

	@Test()
	public void testUpdateRecordNotFound() throws IOException, RecordNotFoundException {
		int expRecNo = 10000000;
		this.expectedEx.expect(RecordNotFoundException.class);
		this.expectedEx.expectMessage(String.format("rec. no.[%s] doesn't exist in db file!", expRecNo));
		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);

		String[] record = new String[]{this.getString(32, "subcontractor"), 
				this.getString(64, "city"), 
				this.getString(64, "types of work"), 
				this.getString(6, "size"), 
				this.getString(8, "rate"), 
				this.getString(8, "owner")};

		handler.updateRecord(expRecNo, 
				record);
		handler.printConsole();
	}

	@Test(expected=IllegalArgumentException.class)
	public void testUpdateRecordInvalidDataLength() throws IOException, RecordNotFoundException {

		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);

		String[] record = new String[]{this.getString(32, "subcontractor"), 
				this.getString(64, "city")};

		handler.updateRecord(1, 
				record);
		handler.printConsole();
	}


	@Test()
	public void testReadRecord() throws IOException, RecordNotFoundException {

		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);

		String[] expRecord = new String[]{this.getString(32, "subcontractor"), 
				this.getString(64, "city"), 
				this.getString(64, "types of work"), 
				this.getString(6, "size"), 
				this.getString(8, "rate"), 
				this.getString(8, "owner")};

		handler.updateRecord(1, 
				expRecord);

		String[] actRecord = handler.readRecord(1);
		for (int i = 0 ; i < expRecord.length ; i ++) {
			Assert.assertEquals(expRecord[i], actRecord[i]);	
		}

	}

	@Test()
	public void testDeleteRecord() throws IOException, RecordNotFoundException {

		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);

		String[] record = handler.readRecord(1);
		Assert.assertEquals(1, handler.findByCriteria(record).length);
		handler.deleteRecord(1);
		handler.findByCriteria(record);
		Assert.assertEquals(0, handler.findByCriteria(record).length);
	}

	@Test()
	public void testDeleteRecordNotFound() throws IOException, RecordNotFoundException {
		this.expectedEx.expect(RecordNotFoundException.class);
		this.expectedEx.expectMessage("rec. no.[1] is marked as deleted in db file!");

		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);


		handler.deleteRecord(1);
		handler.deleteRecord(1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFindByCriteriaOfInvalidLength() throws IOException, RecordNotFoundException {
		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);


		handler.printConsole();
		Assert.assertEquals(0, handler.findByCriteria(new String[] {"match nothing"}).length);
	}

	@Test()
	public void testFindByCriteria() throws IOException, RecordNotFoundException {
		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);


		handler.printConsole();
		Assert.assertEquals(1, handler.findByCriteria(new String[] {null, "Lend","Roof",null, null, null }).length);

		Assert.assertEquals(5, handler.findByCriteria(new String[] {"Moore", null, null, null, null, null }).length);

		Assert.assertEquals(5, handler.findByCriteria(new String[] {"Moore P", null, null, null, null, null }).length);

		Assert.assertEquals(5, handler.findByCriteria(new String[] {"Moore P", null, null, null, "$", "" }).length);
		Assert.assertEquals(0, handler.findByCriteria(new String[] {"Moore P", null, null, null, "85", "" }).length);

		Assert.assertEquals(0, handler.findByCriteria(new String[] {"Moore P", null, null, null, null, "badowner" }).length);
	}

	@Test
	public void testCreateRecord() throws IOException, DuplicateKeyException {
		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);

		handler.printConsole();
		String[] record = new String[]{this.getString(32, "ABC"), 
				this.getString(64, "ABCD"), 
				this.getString(64, "ABCDE"), 
				null, 
				null, 
				null};
		handler.createRecord(record);
		handler.printConsole();
	}

	@Test(expected= DuplicateKeyException.class)
	public void testCreateRecord_Duplicated() throws IOException, DuplicateKeyException {
		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);

		handler.printConsole();
		String[] record = new String[]{this.getString(32, "ABC"), 
				this.getString(64, "ABCD"), 
				this.getString(64, "ABCDE"), 
				null, 
				null, 
				null};
		handler.createRecord(record);
		handler.createRecord(record);
		handler.printConsole();
	}


	@Test()
	public void testCreateRecord_InvalidData() throws IOException, DuplicateKeyException {
		this.expectedEx.expect(IllegalArgumentException.class);
		this.expectedEx.expectMessage("location cannot be null or empty!");
		File destFile = new File("db-2x1-test.db");
		FileUtils.copyFile((new File("db-2x1.db")), destFile);
		DataFileHandler handler = new DataFileHandler(destFile);

		handler.printConsole();
		handler.createRecord(new String[] {"Buonarotti & Company            ", null, "Air Conditioning, Painting, Painting                            ", null, null, null});
		handler.printConsole();
	}

	@Test
	public void testGetString() {
		String prefix = "1234";
		Assert.assertEquals(5, this.getString(5, prefix).length());
	}

	public String getString(int expSizeInBytes, String prefix) {
		int numOfspaces = 0;
		try {
			numOfspaces = expSizeInBytes - prefix.getBytes("US-ASCII").length;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		String result = prefix;
		for (int i = 0 ; i < numOfspaces ; i ++) {
			result += " ";
		}
		return result;
		//return String.format("%-" + numOfspaces + "s", prefix);	
	}

	@Test
	public void testCreateOrGet() throws IOException, InterruptedException {
		DataFileHandler handler = DataFileHandler.getInstance(new File("db-2x1-test.db"));
		
		int size = 1000;
		Thread[] threads = new Thread[size];
		for (int i = 0 ; i < size ; i ++) {
			threads[i] = new Conccurent(handler);
		}
		for (Thread runnable : threads) {
			runnable.start();
		}
		for (Thread runnable : threads) {
			runnable.join();
		}
	}
	
	class Conccurent extends Thread {
		
		private DataFileHandler handler;
		
		public Conccurent(DataFileHandler handler) {
			this.handler = handler;
		}

		public void run() {
			try {
				DataFileHandler newHandler = DataFileHandler.getInstance(new File("db-2x1-test.db"));
				if (handler != null) {
					if (! handler.equals(newHandler)) {
						System.out.println("not euqual!!!");
					}
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}

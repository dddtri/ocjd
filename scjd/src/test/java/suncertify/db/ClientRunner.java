package suncertify.db;

import java.io.IOException;
import java.util.List;

public interface ClientRunner {

	public void run(List<Thread> ts) throws IOException;
}

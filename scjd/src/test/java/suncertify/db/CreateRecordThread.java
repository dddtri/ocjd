package suncertify.db;

import java.io.File;
import java.io.IOException;
import java.util.Random;
public class CreateRecordThread extends Thread {

	@Override
	public void run() {
		DBAccess data = null;
		try {
			data = new Data(new File("db-2x1-test.db"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}			
		try {
			data.createRecord(new String[]{
					"name_" + DataTest.generateString(new Random(), "abcdefghijklmnopqrstuvwxyz", 27),
					"city_" + DataTest.generateString(new Random(), "abcdefghijklmnopqrstuvwxyz", 59),
					"typeofwork_" + DataTest.generateString(new Random(), "abcdefghijklmnopqrstuvwxyz", 53),
					DataTest.generateString(new Random(), "1234567890", 6),
					"$" + DataTest.generateString(new Random(), "1234567890", 7),
					DataTest.generateString(new Random(), "1234567890", 8)
					});
		} catch (DuplicateKeyException e) {
			e.printStackTrace();
		}
	}

	
}

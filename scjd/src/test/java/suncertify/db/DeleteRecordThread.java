package suncertify.db;

import java.io.File;
import java.io.IOException;

public class DeleteRecordThread extends Thread {

	private static Integer recNumCount = 0;

	@Override
	public void run() {
		DBAccess data = null;
		try {
			data = new Data(new File("db-2x1-test.db"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		int recNum = -1;
		synchronized (DeleteRecordThread.class) {
				recNumCount ++;
				recNum = recNumCount;
		}
		try {
			long cookie = data.lockRecord(recNum);
			data.deleteRecord(recNum, cookie);
			data.unlock(recNum, cookie);
		} catch (SecurityException e) {
			throw new AssertionError(e);
		} catch (RecordNotFoundException e) {
			throw new AssertionError(e);
		}
	}


}

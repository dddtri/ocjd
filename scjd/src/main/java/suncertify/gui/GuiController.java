package suncertify.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import suncertify.BusinessServiceAdaptor;
import suncertify.Mode;

/**
 * A {@code GuiController} handles events that occurred on the {@code MainWindow}(Records Table) screen
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class GuiController implements TableModelListener, ActionListener, ItemListener {
	
	/**
	 * A {@code Logger} that logs useful information
	 */
	static private Logger logger = Logger.getLogger(GuiController.class.getName());
	
	/**
	 * A {@code MainWindow} that contains UI components for backtracking  
	 * source of a given event
	 */
	private MainWindow window;
	
	/**
	 * A {@code BusinessServiceAdaptor} for business logic process
	 */
	private BusinessServiceAdaptor service;
	
	/**
	 * A constructor
	 * 
	 * @param mode	a {@code Mode} that specifies the type of mode to run 
	 * @throws IOException thrown if any issue occurs with operating on data base file
	 * @throws NotBoundException thrown if an attempt is made to lookup or unbind in the registry 
	 * 							 a name that has no associated binding. 
	 */
	public GuiController(Mode mode) throws IOException, NotBoundException {
		this.service = new BusinessServiceAdaptor(mode);
	}

	/**
	 * Set {@code MainWindow}
	 * 
	 * @param window	a {@code MainWindow}
	 */
	public void setMainWindow(MainWindow window) {
		this.window = window;
	}

	/**
	 * Process update logic 
	 */
	public void tableChanged(TableModelEvent e) {
		int columnIndex = e.getColumn();
		int rowIndex = e.getFirstRow();
		logger.entering(GuiController.class.getName(), "tableChanged", rowIndex + ":" + columnIndex);
		
		TableModel tableModel = (TableModel) e.getSource();
		
		int columnSize = tableModel.getColumnCount();
		String[] updatedRecord = new String[columnSize];
		for (int i = 0 ; i < columnSize ; i ++) {
			updatedRecord[i] = (String) tableModel.getValueAt(rowIndex, i); 
		}

		this.service.updateRecord(updatedRecord);
		
		logger.exiting(GuiController.class.getName(), "tableChanged");
	}

	/**
	 * Process filter logic
	 */
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		
		JButton searchButton = this.window.getSearchButton();
		JTextField locField = this.window.getLocationField();
		JTextField nameField = this.window.getNameField();
		JRadioButton andOption = this.window.getAndOption();
		JRadioButton orOption = this.window.getOrOption();
		
		if (!(source == searchButton)) {
			logger.throwing(GuiController.class.getName(), "actionPerformed", 
					new UnsupportedOperationException(String.format("action performed for source[%s] is not supported!", 
							source.getClass().getName())));
			return;
		}
		
		boolean isAndSelected = andOption.isSelected();
		boolean isOrSelected = orOption.isSelected();
		String location = RecordsTableModel.getFormattedLocationString(locField.getText().trim());
		String name = RecordsTableModel.getFormattedNameString(nameField.getText().trim());
		
		String[][] filteredData = this.service.searchRecords(isAndSelected, isOrSelected,
				location, name);
		
		this.window.getTable().setModel(new RecordsTableModel(this.service.getFieldNames(), filteredData));
		this.window.getTable().revalidate();
		//register listener once again after model replacement
		this.window.getTable().getModel().addTableModelListener(this);
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	public void itemStateChanged(ItemEvent e) {
		//empty
	}

	/**
	 * Gets {@code BusinessServiceAdaptor}
	 * 
	 * @return	a {@code BusinessServiceAdaptor}
	 */
	public BusinessServiceAdaptor getService() {
		return this.service;
	}
	
	

}

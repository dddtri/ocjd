package suncertify.gui;

import javax.swing.table.AbstractTableModel;

/**
 * A customized {@code TableModel} for contractor records specific table.  This {@code TableModel} restricts 
 * user from updating all data except data in "owner" field that is meant for "Book" operation
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class RecordsTableModel extends AbstractTableModel {

	/**
	 * a serial version uid in {@code long}
	 */
	private static final long serialVersionUID = -5528431766886212072L;

	/**
	 * a {@code String[]} containing the field names to be displayed onto the table header.
	 */
	private String[] colNames;

	/**
	 * a {@code String[][]} containing rows of record data to be displayed onto the table.
	 */
	private String[][] tableRecords;
	
	

	/**
	 * A constructor
	 * 
	 * @param colNames		a {@code String[]} containing the field names to be displayed onto
	 * 						the table header.  Must not be {@code null}
	 * 
	 * @param tableRecords	a {@code String[][]} containing rows of record data to be displayed onto 
	 * 						the table.  Must not be {@code null}
	 */
	public RecordsTableModel(String[] colNames, String[][] tableRecords) {
		if (colNames == null) {
			throw new NullPointerException("colNames cannot be null!");
		}
		if (tableRecords == null) {
			throw new NullPointerException("tableRecords cannot be null!");
		}
		this.colNames = colNames;
		this.tableRecords = tableRecords;
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() {
		return this.tableRecords.length;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return this.colNames.length;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	public String getColumnName(int columnIndex) {
		if (columnIndex < 0 || columnIndex > this.colNames.length - 1) {
			throw new IllegalArgumentException("columnIndex must not be negative or out of boundary!");
		}

		return this.colNames[columnIndex];

	}


	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex < 0 || rowIndex > this.tableRecords.length - 1) {
			throw new IllegalArgumentException("rowIndex must not be negative or out of boundary!");
		}
		if (columnIndex < 0 || columnIndex > this.colNames.length - 1) {
			throw new IllegalArgumentException("columnIndex must not be negative or out of boundary!");
		}
		return this.tableRecords[rowIndex][columnIndex];
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
	 */
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (rowIndex < 0 || rowIndex > this.tableRecords.length - 1) {
			throw new IllegalArgumentException("rowIndex must not be negative or out of boundary!");
		}
		if (columnIndex < 0 || columnIndex > this.colNames.length - 1) {
			throw new IllegalArgumentException("columnIndex must not be negative or out of boundary!");
		}

		if (columnIndex < colNames.length - 1) {
			return false;
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
	 */
	public void setValueAt(Object aValue, int row, int column) {
		//only 1 column (owner) is editable, so safely restrict the value to 8 bytes without checking the column 
		//position
		String prefix = ((String) aValue).trim();

		this.tableRecords[row][column] = getFormattedOwnerString(prefix);
		fireTableCellUpdated(row, column);
	}

	/**
	 * Gets value that ensures 8 bytes {@code String} with space padding on the right for owner 
	 * field value
	 *  
	 * @param prefix	a {@code String} containing value to be formatted.  Provided value 
	 * 					cannot be {@code null}.  In addition, if provided value is greater than 
	 * 					8 bytes, the returned value will be trimmed down to 8 bytes
	 * @return	 a 8 bytes {@code String} with space padding on the right
	 */
	public static String getFormattedOwnerString(String prefix) {
		if (prefix == null) {
			throw new NullPointerException("prefix cannot be null!");
		}
		
		//take the first 8 bytes if length exceeds
		if (prefix.length() > 8) {
			prefix = prefix.substring(0, 8);
		}
		
		//right padding
		return String.format("%1$-8s", prefix);
	}
	

	/**
	 * Gets value that ensures 64 bytes {@code String} with space padding on the right for location 
	 * field value
	 *  
	 * @param prefix	a {@code String} containing value to be formatted.  Provided value 
	 * 					cannot be {@code null}.  In addition, if provided value is greater than 
	 * 					8 bytes, the returned value will be trimmed down to 8 bytes
	 * @return	 a 64 bytes {@code String} with space padding on the right
	 */
	public static String getFormattedLocationString(String prefix) {
		if (prefix == null) {
			throw new NullPointerException("prefix cannot be null!");
		}
		
		//take the first 8 bytes if length exceeds
		if (prefix.length() > 64) {
			prefix = prefix.substring(0, 64);
		}
		
		//right padding
		return String.format("%1$-64s", prefix);
	}
	
	/**
	 * Gets value that ensures 32 bytes {@code String} with space padding on the right for name 
	 * field value
	 *  
	 * @param prefix	a {@code String} containing value to be formatted.  Provided value 
	 * 					cannot be {@code null}.  In addition, if provided value is greater than 
	 * 					8 bytes, the returned value will be trimmed down to 8 bytes
	 * @return	 a 32 bytes {@code String} with space padding on the right
	 */	
	public static String getFormattedNameString(String prefix) {
		if (prefix == null) {
			throw new NullPointerException("prefix cannot be null!");
		}
		
		//take the first 8 bytes if length exceeds
		if (prefix.length() > 32) {
			prefix = prefix.substring(0, 32);
		}
		
		//right padding
		return String.format("%1$-32s", prefix);
	}
	
}

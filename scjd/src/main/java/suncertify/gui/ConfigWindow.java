package suncertify.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import suncertify.DatabaseConfig;
import suncertify.Mode;

/**
 * A {@code ConfigWindow} that is responsible for the opening configuration window display.  The 
 * UI components within are displayed based on selected {@code Mode}
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class ConfigWindow extends JFrame {

	/**
	 * a serial version uid in {@code long} 
	 */
	private static final long serialVersionUID = 3713659103148161558L;
	
	/**
	 * A location label of {@code JLabel} type accessible by associated controller
	 */
	private JLabel dbLocationLabel;
	
	/**
	 * A location text field of {@code JTextField} type accessible by associated controller
	 */
	private JTextField dbLocationField;
	
	/**
	 * A file selection button of {@code JButton} type accessible by associated controller
	 */
	private JButton fileChooseButton;
	
	/**
	 * A file selector of {@code JFileChooser} type accessible by associated controller
	 */
	private JFileChooser fileChooser;
	
	/**
	 * A port label of {@code JLabel} type accessible by associated controller
	 */	
	private JLabel portLabel;
	
	/**
	 * A port field of {@code JTextField} type accessible by associated controller
	 */
	private JTextField portField;
	
	/**
	 * A ip label of {@code JLabel} type accessible by associated controller
	 */	
	private JLabel ipLabel;
	
	/**
	 * A ip field of {@code JTextField} type accessible by associated controller
	 */
	private JTextField ipField;
	
	/**
	 * A submit button of {@code JButton} type accessible by associated controller
	 */
	private JButton connButton;

	/**
	 * A {@code DatabaseConfig} that reference suncertify.properties for settings persistence
	 */
	private DatabaseConfig dbCfg;
	
	/**
	 * A {@code mode} that specifies in which mode the given application should display the 
	 * UI components
	 */
	private Mode mode;

	/**
	 * The main entry point for configuration window display
	 * 
	 * @param args	a {@code String[]} that is expected to contain a {@code Mode} value  
	 */
	public static void main(String[] args) {
		Logger.getLogger("").getHandlers()[0].setLevel(Level.ALL);
		Logger.getLogger("suncertify").setLevel(Level.ALL);
		if (args.length > 1) {
			System.err.println("Only 1 parameter allows where the value only accepts either \"alone\" or \"server\"! ");
			System.exit(1);
		}

		Mode mode = Mode.CLIENT;
		if (args.length == 1) {
			String modeStr = args[0].trim();
			try {
				mode = Mode.valueOf(modeStr.toUpperCase());
			} catch (Exception e) {
				System.err.println("Only 1 parameter allows where the value only accepts either \"alone\" or \"server\"! ");
				System.err.println(String.format("you have entered [%s]", modeStr));
				System.exit(1);			
			}			
		}

		final Mode selectedMode = mode;

		javax.swing.SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				ConfigWindow.createAndShowGUI(selectedMode);
			}

		});


	}
	
	
	/**
	 * A constructor
	 * 
	 * @param mode		a {@code Mode} that indicates the mode in which the application should be ran
	 */
	public ConfigWindow(Mode mode) {
		super("Environment setup for " + mode.name() + " mode");

		this.mode = mode;
		this.dbCfg = new DatabaseConfig();
		this.newComponents();
		this.registerListeners();
	}

	/**
	 * Registers listeners(the controller) to the {@code JComponent}s in interest
	 */
	private void registerListeners() {
		ConfigWindowListener listner = new ConfigWindowListener(this);
		this.connButton.addActionListener(listner);
		this.fileChooseButton.addActionListener(listner);

	}
	
	/**
	 * Creates {@code JComponent}s that are exposed to controller for business logic processing
	 */
	private void newComponents() {

		Properties prpties = this.dbCfg.load();

		this.dbLocationLabel = new JLabel("Database location");
		this.dbLocationField = new JTextField(32);
		this.fileChooseButton = new JButton("...");
		this.fileChooser = new JFileChooser();
		this.portLabel = new JLabel("Server port");
		this.portField = new JTextField(8);
		this.ipLabel = new JLabel("Ip address");
		this.ipField = new JTextField(8);
		this.connButton = new JButton("connect");
		
		if (Mode.ALONE == this.mode) {
			//null passing in is equivalent to passing in empty string
			this.dbLocationField.setText(prpties.getProperty(DatabaseConfig.PRPTY_NAME_MODE_STANDALONE_DB_FILE_PATH));	
		}
		if (Mode.SERVER == this.mode) {
			//null passing in is equivalent to passing in empty string
			this.dbLocationField.setText(prpties.getProperty(DatabaseConfig.PRPTY_NAME_MODE_SERVER_DB_FILE_PATH));
			this.portField.setText(prpties.getProperty(DatabaseConfig.PRPTY_NAME_MODE_SERVER_PORT));
			this.ipField.setText(prpties.getProperty(DatabaseConfig.PRPTY_NAME_MODE_SERVER_IP_ADDRESS));
		}
		if (Mode.CLIENT == this.mode) {
			//null passing in is equivalent to passing in empty string
			this.portField.setText(prpties.getProperty(DatabaseConfig.PRPTY_NAME_MODE_CLIENT_PORT));
			this.ipField.setText(prpties.getProperty(DatabaseConfig.PRPTY_NAME_MODE_CLIENT_IP_ADDRESS));
		}
		
	}
	/**
	 * Composes a {@code JFrame} and display GUI
	 */
	public static void createAndShowGUI(Mode mode) {
		ConfigWindow frame = new ConfigWindow(mode);
		JPanel panel = frame.createFieldsPanel();
		JPanel buttonsPanel = frame.createButtonsPanel();


		frame.setLayout(new BorderLayout());
		frame.add(panel, BorderLayout.NORTH);
		frame.add(buttonsPanel, BorderLayout.SOUTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);




		frame.pack();
		frame.setVisible(true);
	}
	
	/**
	 * Composes and creates a fields panel based on selected {@code Mode}
	 * 
	 * @return	a {@code JPanel} that contains fields {@code Component}s 
	 */
	private JPanel createFieldsPanel() {
		JPanel panel = new JPanel();		
		GridBagLayout gridbag = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		panel.setLayout(gridbag);
		constraints.insets = new Insets(2, 2, 2, 2);
		
		if (Mode.SERVER == this.mode || Mode.ALONE == this.mode) {
			gridbag.setConstraints(this.dbLocationLabel, constraints);
			panel.add(this.dbLocationLabel);
			gridbag.setConstraints(this.dbLocationField, constraints);
			panel.add(this.dbLocationField);
	
			constraints.gridwidth = GridBagConstraints.REMAINDER;
			gridbag.setConstraints(this.fileChooseButton, constraints);
			panel.add(this.fileChooseButton);
		}

		if (Mode.SERVER == this.mode || Mode.CLIENT == this.mode) {
			constraints.gridwidth = 1;
			gridbag.setConstraints(this.ipLabel, constraints);
			panel.add(this.ipLabel);
			constraints.gridwidth = GridBagConstraints.REMAINDER;
			constraints.anchor = GridBagConstraints.WEST;
			gridbag.setConstraints(this.ipField, constraints);
			panel.add(this.ipField);
			
			
			constraints.gridwidth = 1;
			gridbag.setConstraints(this.portLabel, constraints);
			panel.add(this.portLabel);
			constraints.gridwidth = GridBagConstraints.REMAINDER;
			constraints.anchor = GridBagConstraints.WEST;
			gridbag.setConstraints(this.portField, constraints);
			panel.add(this.portField);
		}

		return panel;
	}

	/**
	 * Composes and creates a buttons panel
	 * 
	 * @return	a {@code JPanel} that contains button {@code Component}s 
	 */
	private JPanel createButtonsPanel() {
		JPanel panel = new JPanel();		
		panel.setLayout(new FlowLayout());
		panel.add(connButton);
		return panel;
	}
	
	/**
	 * Gets {@code JLabel}  that corresponds to the database file path label in the fields panel
	 * @return a {@code JLabel}  that corresponds to the database file path label in the fields panel
	 */
	public JLabel getDbLocationLabel() {
		return dbLocationLabel;
	}

	/**
	 * Gets {@code JTextField}  that corresponds to the database file path field in the fields panel
	 * @return a {@code JTextField}  that corresponds to the database file path field in the fields panel
	 */
	public JTextField getDbLocationField() {
		return dbLocationField;
	}
	
	/**
	 * Gets {@code JButton}  that corresponds to the file selector button in the fields panel
	 * @return a {@code JButton}  that corresponds to the file selector button in the fields panel
	 */
	public JButton getFileChooseButton() {
		return fileChooseButton;
	}
	
	/**
	 * Gets {@code JFileChooser} 
	 * @return a {@code JFileChooser}
	 */	
	public JFileChooser getFileChooser() {
		return fileChooser;
	}
	
	/**
	 * Gets {@code JLabel}  that corresponds to the port label in the fields panel
	 * @return a {@code JLabel}  that corresponds to the port label in the fields panel
	 */
	public JLabel getPortLabel() {
		return portLabel;
	}
	
	/**
	 * Gets {@code JTextField}  that corresponds to the port field in the fields panel
	 * @return a {@code JTextField}  that corresponds to the port field in the fields panel
	 */
	public JTextField getPortField() {
		return portField;
	}
	
	/**
	 * Gets {@code JLabel}  that corresponds to the ip label in the fields panel
	 * @return a {@code JLabel}  that corresponds to the ip label in the fields panel
	 */
	public JLabel getIpLabel() {
		return ipLabel;
	}
	
	/**
	 * Gets {@code JTextField}  that corresponds to the ip field in the fields panel
	 * @return a {@code JTextField}  that corresponds to the ip field in the fields panel
	 */
	public JTextField getIpField() {
		return ipField;
	}
	

	/**
	 * Gets {@code JButton}  that corresponds to the submit button in the buttons panel
	 * @return a {@code JButton}  that corresponds to the submit button in the buttons panel
	 */
	public JButton getConnButton() {
		return connButton;
	}
	
	/**
	 * Gets a {@code DatabaseConfig}
	 * @return	a {@code DatabaseConfig}
	 */
	public DatabaseConfig getDbCfg() {
		return dbCfg;
	}
	
	/**
	 * Gets {@code Mode}
	 * @return	a {@code Mode}
	 */
	public Mode getMode() {
		return mode;
	}


}


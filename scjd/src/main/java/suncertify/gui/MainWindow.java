package suncertify.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.rmi.NotBoundException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import suncertify.Mode;

/**
 * A {@code MainWindow} handles Records table screen display  
 * @author Daniel (ShenYu) Shih
 *
 */
public class MainWindow extends JFrame {

	/**
	 * a serial version uid of {@code long} type
	 */
	private static final long serialVersionUID = 3601102558550411822L;

	/**
	 * A table of {@code JTable} type accessible by associated controller
	 */
	private JTable table;
	
	/**
	 * A button of {@code JButton} type accessible by associated controller
	 */
	private JButton searchButton;
	
	/**
	 * A radio button of {@code JRadioButton} type accessible by associated controller
	 */	
	private JRadioButton andOption;
	
	/**
	 * A radio button of {@code JRadioButton} type accessible by associated controller
	 */	
	private JRadioButton orOption;

	/**
	 * A search field of {@code JTextField} type accessible by associated controller
	 */
	private JTextField nameField;
	
	/**
	 * A search field of {@code JTextField} type accessible by associated controller
	 */
	private JTextField locationField;

	/**
	 * A search field label of {@code JTextField} type accessible by associated controller
	 */
	private JLabel nameLabel;
	
	/**
	 * A search field label of {@code JTextField} type accessible by associated controller
	 */
	private JLabel locLabel;;

	/**
	 * A {@code GuiController} to be registered to the UI components for 
	 * business logic process
	 */
	private GuiController controller;

	/**
	 * A debug flag for internal debug purpose
	 */
	private boolean DEBUG = false;

	/**
	 * A constructor
	 * 
	 * @param mode		a {@code Mode} that indicates the mode in which the application should be ran
	 * @throws IOException thrown if any issue occurs with operating on data base file
	 * @throws NotBoundException thrown if an attempt is made to lookup or unbind in the registry a name 
	 * 						     that has no associated binding.
	 */
	public MainWindow(Mode mode) throws IOException, NotBoundException {
		super("Contractors Booking Application[" + mode + " mode]");
		this.controller = new GuiController(mode);
		controller.setMainWindow(this);
		this.newComponents();
		this.registerListeners();
	}

	/**
	 * Creates {@code JComponent}s that are exposed to controller for business logic processing
	 */
	private void newComponents() {
		//table
		String[][] allRecords = this.controller.getService().searchRecords(true, false, null, null);
		String[] fieldNames = this.controller.getService().getFieldNames();
		this.table = new JTable(new RecordsTableModel(fieldNames, allRecords));
		this.table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		this.table.setFillsViewportHeight(true);


		//button
		this.searchButton = new JButton("search");

		//radio button
		String andStr = "and";
		this.andOption = new JRadioButton(andStr);
		this.andOption.setActionCommand(andStr);
		String orStr = "or";
		this.orOption = new JRadioButton(orStr);
		this.orOption.setActionCommand(orStr);
		this.orOption.setSelected(true);

		//fields
		this.nameField = new JTextField(32);
		this.locationField = new JTextField(32);

		//field
		this.nameLabel = new JLabel("name");
		this.locLabel = new JLabel("location");

	}

	/**
	 * Registers listeners(the controller) to the {@code JComponent}s in interest
	 */
	private void registerListeners() {
		if (DEBUG) {
			this.table.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					printDebugData(table);
				}
			});
		}

		this.table.getModel().addTableModelListener(this.controller);
		this.andOption.addItemListener(this.controller);
		this.orOption.addItemListener(this.controller);
		this.searchButton.addActionListener(this.controller);
	}

	/**
	 * Composes a {@code JFrame} and display GUI
	 * @throws IOException thrown if any issue occurs with operating on data base file
	 * @throws NotBoundException thrown if an attempt is made to lookup or unbind in the registry a name 
	 * 						     that has no associated binding.
	 */
	public static void createAndShowGUI(Mode mode) throws IOException, NotBoundException {
		MainWindow frame = new MainWindow(mode);
		JPanel p = new JPanel(new BorderLayout());
		JPanel center = frame.createTablePanel();
		JPanel top = frame.createFilterPanel();

		p.add(center, BorderLayout.CENTER);
		p.add(top, BorderLayout.SOUTH);
		p.setOpaque(true);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(p);
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * Composes and creates a records table panel
	 * 
	 * @return	a {@code JPanel} that contains {@code JTable} for records listing and booking process
	 */
	public JPanel createTablePanel() {
		JScrollPane scrollPane = new JScrollPane(this.table);
		JPanel tablePanel = new JPanel(new GridLayout(1,0));
		tablePanel.add(scrollPane);
		return tablePanel;
	}

	/**
	 * Composes and creates a filter panel
	 * 
	 * @return	a {@code JPanel} that contains {@code Component}s for record filtering
	 */
	public JPanel createFilterPanel() {

		ButtonGroup group = new ButtonGroup();
		group.add(this.andOption);
		group.add(this.orOption);

		//north panel
		JPanel northPanel = new JPanel();
		northPanel.add(this.andOption);
		northPanel.add(this.orOption);
		northPanel.add(this.searchButton);

		//south panel
		JPanel southPanel = new JPanel();
		southPanel.add(this.nameLabel);
		southPanel.add(this.nameField);
		southPanel.add(this.locLabel);
		southPanel.add(this.locationField);


		//aggregate components
		JPanel radioPanel = new JPanel(new BorderLayout());
		radioPanel.add(northPanel, BorderLayout.SOUTH);
		radioPanel.add(southPanel, BorderLayout.NORTH);

		return radioPanel;
	}

	/**
	 * Print debugging data in table for debugging purpose
	 * 
	 * @param table	a {@code JtTable}
	 */
	private void printDebugData(JTable table) {
		int numRows = table.getRowCount();
		int numCols = table.getColumnCount();
		TableModel model = table.getModel();

		System.out.println("View of data");
		for (int i = 0 ; i < numRows ; i ++) {
			System.out.print("    row " + i + ":");
			for (int j = 0 ; j < numCols ; j ++) {
				System.out.println("  " + model.getValueAt(i, j));
			}
			System.out.println();
		}
		System.out.println("--------------------------");

	}

	/**
	 * Gets {@code JTable} that corresponds to contractor record table
	 * 
	 * @return a {@code JTable} that corresponds to contractor record table
	 */
	public JTable getTable() {
		return table;
	}

	/**
	 * Gets {@code JButton} that corresponds to the search button in the filter panel
	 * @return a {@code JButton} that corresponds to the search button in the filter panel
	 */
	public JButton getSearchButton() {
		return searchButton;
	}

	/**
	 * Gets {@code JRadioButton}  that corresponds to the and radio button in the filter panel
	 * @return a {@code JRadioButton}  that corresponds to the and radio button in the filter panel
	 */
	public JRadioButton getAndOption() {
		return andOption;
	}

	/**
	 * Gets {@code JRadioButton}  that corresponds to the or radio button in the filter panel
	 * @return a {@code JRadioButton}  that corresponds to the or radio button in the filter panel
	 */
	public JRadioButton getOrOption() {
		return orOption;
	}

	/**
	 * Gets {@code JTextField}  that corresponds to the name field in the filter panel
	 * @return a {@code JTextField}  that corresponds to the name field in the filter panel
	 */
	public JTextField getNameField() {
		return nameField;
	}

	/**
	 * Gets {@code JTextField}  that corresponds to the location field in the filter panel
	 * @return a {@code JTextField}  that corresponds to the location field in the filter panel
	 */
	public JTextField getLocationField() {
		return locationField;
	}

	/**
	 * Gets {@code JLabel}  that corresponds to the name label in the filter panel
	 * @return a {@code JLabel}  that corresponds to the name label in the filter panel
	 */
	public JLabel getNameLabel() {
		return nameLabel;
	}

	/**
	 * Gets {@code JLabel}  that corresponds to the location label in the filter panel
	 * @return a {@code JLabel}  that corresponds to the location label in the filter panel
	 */
	public JLabel getLocLabel() {
		return locLabel;
	}


}

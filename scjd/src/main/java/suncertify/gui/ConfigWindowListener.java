package suncertify.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Properties;
import java.util.logging.Logger;

import javax.swing.JFileChooser;

import suncertify.DatabaseConfig;
import suncertify.Mode;
import suncertify.service.impl.RemoteBusinessServiceImpl;

/**
 * A {@code ConfigWindowListener} handles events that occurred on the {@code ConfigWindow} screen 
 * based on the selected {@code Mode}
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class ConfigWindowListener implements ActionListener {

	/**
	 * A {@code ConfigWindow} that contains UI component for backtracking  
	 * source of a given event
	 */
	private ConfigWindow frame;

	/**
	 * A {@code Logger} that logs useful information
	 */
	private static Logger logger = Logger.getLogger(ConfigWindowListener.class.getName());

	/**
	 * A constructor
	 * 
	 * @param frame	a {@code ConfigWindow}
	 */
	public ConfigWindowListener(ConfigWindow frame) {
		if (frame == null) {
			throw new NullPointerException("frame cannot be null!");
		}
		this.frame = frame;

	}

	/**
	 * Process flow based on selected {@code Mode}
	 */
	public void actionPerformed(ActionEvent e) {
		DatabaseConfig dbCfg = this.frame.getDbCfg();
		if (e.getSource() == this.frame.getConnButton()) {

			//persist properties into suncertify.properties
			Properties prpties = dbCfg.load();
			if (Mode.ALONE == this.frame.getMode()) {
				this.setPrpty(prpties, DatabaseConfig.PRPTY_NAME_MODE_STANDALONE_DB_FILE_PATH, this.frame.getDbLocationField().getText());	
			}
			if (Mode.SERVER == this.frame.getMode()) {
				this.setPrpty(prpties, DatabaseConfig.PRPTY_NAME_MODE_SERVER_DB_FILE_PATH, this.frame.getDbLocationField().getText());
				this.setPrpty(prpties, DatabaseConfig.PRPTY_NAME_MODE_SERVER_IP_ADDRESS, this.frame.getIpField().getText());
				this.setPrpty(prpties, DatabaseConfig.PRPTY_NAME_MODE_SERVER_PORT, this.frame.getPortField().getText());
			}
			if (Mode.CLIENT == this.frame.getMode()) {
				this.setPrpty(prpties, DatabaseConfig.PRPTY_NAME_MODE_CLIENT_IP_ADDRESS, this.frame.getIpField().getText());
				this.setPrpty(prpties, DatabaseConfig.PRPTY_NAME_MODE_CLIENT_PORT, this.frame.getPortField().getText());
			}
			dbCfg.store();

			//alone or client mode brings up client gui
			if (Mode.ALONE == this.frame.getMode() || Mode.CLIENT == this.frame.getMode()) {
				try {
					MainWindow.createAndShowGUI(this.frame.getMode());

					this.frame.setVisible(false);
					this.frame.dispose();
				} catch (Exception er) {
					logger.throwing(ConfigWindowListener.class.getName(), "actionPerformed", er);
				}
			}
			
			
			//server mode starts up rmi server
			if (Mode.SERVER == this.frame.getMode()) {
				try {
					RemoteBusinessServiceImpl.startService(
							prpties.getProperty(DatabaseConfig.PRPTY_NAME_MODE_SERVER_IP_ADDRESS, "127.0.0.1"), 
							Integer.parseInt(prpties.getProperty(DatabaseConfig.PRPTY_NAME_MODE_SERVER_PORT, "1099")));
					
					this.frame.setVisible(false);
					this.frame.dispose();					
				} catch (Exception er) {
					logger.throwing(ConfigWindowListener.class.getName(), "actionPerformed", er);
				}

			}
		}

		//file selection event
		if (e.getSource() == this.frame.getFileChooseButton()) {
			int status = this.frame.getFileChooser().showOpenDialog(this.frame);
			if (status == JFileChooser.APPROVE_OPTION) {
				File file =  this.frame.getFileChooser().getSelectedFile();
				this.frame.getDbLocationField().setText(file.getAbsolutePath());
			}
		}
	}

	/**
	 * Sets properties
	 * 
	 * @param prpties	a {@code Properties} that collects specified property key and value
	 * @param prptyName	a {@code String} containing the property key
	 * @param dbPath	a {@code String} containing the property value
	 */
	private void setPrpty(Properties prpties, String prptyName, String dbPath) {
		if (dbPath != null && dbPath.length() > 0) {
			prpties.put(prptyName, dbPath);
		}
	}

}

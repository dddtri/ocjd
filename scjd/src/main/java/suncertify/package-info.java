/**
 * the top most package that holds the service adoptor class for the client as well as
 * the class that retrieves and updates the suncertify.properties
 */
package suncertify;

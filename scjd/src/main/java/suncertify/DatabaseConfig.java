package suncertify;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * A {@code DatabaseConfig} that persists settings specified in the configuration window GUI 
 * from previous and current session.
 *  
 * @author Daniel (ShenYu) Shih
 *
 */
public class DatabaseConfig {
	
	/**
	 * A {@code file} that is the "suncertify.properties" property file
	 */
	private File propertyFile;
	
	/**
	 * A {@code String} containing the current working directory
	 */
	private static final String CURRENT_WORKING_DIR = System.getProperty("user.dir");
	
	/**
	 * A {@code String} containing the property file name 
	 */
	private static final String PROPERTY_FILE_NAME = "suncertify.properties";
	
	/**
	 * A {@code String} containing key name for save/retrieve database file path value in alone mode opening gui 
	 */
	public static final String PRPTY_NAME_MODE_STANDALONE_DB_FILE_PATH = "mode.standalone.db.file.path";

	/**
	 * A {@code String} containing key name for save/retrieve port value in client mode opening gui 
	 */
	public static final String PRPTY_NAME_MODE_CLIENT_PORT = "mode.client.serverport";

	/**
	 * A {@code String} containing key name for ip address value in client mode opening gui 
	 */
	public static final String PRPTY_NAME_MODE_CLIENT_IP_ADDRESS = "mode.client.serveripaddress";;

	/**
	 * A {@code String} containing key name for save/retrieve port value in server mode opening gui 
	 */
	public static final String PRPTY_NAME_MODE_SERVER_PORT = "mode.server.port";

	/**
	 * A {@code String} containing key name for save/retrieve ip address value in server mode opening gui 
	 */
	public static final String PRPTY_NAME_MODE_SERVER_IP_ADDRESS = "mode.server.ipaddress";

	/**
	 * A {@code String} containing key name for save/retrieve database file path value in server mode opening gui 
	 */
	public static final String PRPTY_NAME_MODE_SERVER_DB_FILE_PATH = "mode.server.db.file.path";
	
	/**
	 * A {@code Properties} containing settings persisted in the memory
	 */
	private Properties properties;
	
	/**
	 * A {@code Logger} that logs useful info.
	 */
	private static Logger logger = Logger.getLogger(DatabaseConfig.class.getName());
	
	/**
	 * A default constructor
	 */
	public DatabaseConfig() {
		this.propertyFile = new File(CURRENT_WORKING_DIR + File.separator + PROPERTY_FILE_NAME);  
	}
	
	/**
	 * Loads suncertify.properties into memory.
	 *  
	 * @return	a {@code Properties} containing the properties archived in suncertify.properties. If 
	 * 			the property file doesn't exist during the load-up or any issue with load operation occurs, 
	 * 			a empty {@code Properties} is returned
	 */
	public Properties load() {
		this.properties = new Properties();
		//return empty properties if no given file exist
		if (! this.propertyFile.exists()) {
			logger.exiting(DatabaseConfig.class.getName(), "load", this.properties);
			return properties;
		}
		
		//otherwise, return loaded properties
		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(this.propertyFile);
			properties.load(inputStream);
			inputStream.close();
		} catch (FileNotFoundException e) {
			logger.throwing(DatabaseConfig.class.getName(), "load", e);
		} catch (IOException e) {
			logger.throwing(DatabaseConfig.class.getName(), "load", e);
		}
		
		logger.exiting(DatabaseConfig.class.getName(), "load", this.properties);
		return properties;
	}
	
	
	/**
	 * Updates suncertify.properties by storing {@code Properties} in memory into the file.
	 *  
	 * @return	a {@code boolean} indicating if the store operation is successful.
	 */
	public boolean store() {
		if (properties == null) {
			throw new NullPointerException("properties cannot be null!");
		}

		if (! this.propertyFile.exists()) {
			try {
				this.propertyFile.createNewFile();
			} catch (IOException e) {
				logger.throwing(DatabaseConfig.class.getName(), "store", e);
				return false;
			}
		}
		
		FileOutputStream inputStream = null;
		try {
			inputStream = new FileOutputStream(this.propertyFile);
			properties.store(inputStream, "");
			inputStream.close();
		} catch (FileNotFoundException e) {
			logger.throwing(DatabaseConfig.class.getName(), "store", e);
			return false;
		} catch (IOException e) {
			logger.throwing(DatabaseConfig.class.getName(), "store", e);
			return false;
		}
		
		return true;
	}
}

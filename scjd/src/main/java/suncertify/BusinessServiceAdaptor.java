package suncertify;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Properties;
import java.util.logging.Logger;

import suncertify.gui.GuiController;
import suncertify.service.BusinessService;
import suncertify.service.RemoteBusinessService;
import suncertify.service.impl.LocalBusinessServiceImpl;
import suncertify.service.impl.RemoteBusinessServiceImpl;

/**
 * A {@code BusinessServiceAdaptor} is an adaptor that provides business logic to the client of alone 
 * or client mode 
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class BusinessServiceAdaptor {

	/**
	 * A {@code Logger} that logs useful information
	 */
	static private Logger logger = Logger.getLogger(GuiController.class.getName());
	
	
	/**
	 * A {@code BusinessService} that provides business logic implementation
	 */
	private BusinessService service;
	

	/**
	 * A constructor
	 * 
	 * @param mode	a {@code Mode} that specifies the type of mode to run 
	 * @throws IOException thrown if any issue occurs with operating on data base file
	 * @throws NotBoundException thrown if an attempt is made to lookup or unbind in the registry 
	 * 							 a name that has no associated binding. 
	 */
	public BusinessServiceAdaptor(Mode mode) throws IOException, NotBoundException {
		
		logger.entering(BusinessServiceAdaptor.class.getName(), "BusinessServiceAdaptor", mode);
		switch (mode) {
			case ALONE: {
				this.service = new LocalBusinessServiceImpl();
				
				break;
			}
			case CLIENT: {
				DatabaseConfig dbConfig = new DatabaseConfig();
				Properties prpties = dbConfig.load();
				Registry reg = LocateRegistry.getRegistry(
						prpties.getProperty(DatabaseConfig.PRPTY_NAME_MODE_CLIENT_IP_ADDRESS, "127.0.0.1"), 
						Integer.parseInt(prpties.getProperty(DatabaseConfig.PRPTY_NAME_MODE_CLIENT_PORT, "1099")));
				this.service = (RemoteBusinessService) reg.lookup(RemoteBusinessServiceImpl.RMI_BIND_NAME);
				break;
			}
			default: {
				throw new UnsupportedOperationException(String.format("Given mode[%s] isn't supported!", mode));
			}
		}
		logger.exiting(BusinessServiceAdaptor.class.getName(), "BusinessServiceAdaptor", this.service);
		
	}
	
	/**
	 * Update record based on foreign key of provided record
	 * 
	 * @param updatedRecord a {@code String[]} that contains updated version of record to 
	 * 						be saved into the db file
	 *  
	 */
	public void updateRecord(String[] updatedRecord) {
		try {
			this.service.updateRecord(updatedRecord);
		} catch (RemoteException e) {
			logger.throwing(BusinessServiceAdaptor.class.getName(), "updateRecord", e);
		}
	}
	
	/**
	 * Search records based on specified parameters. {@code IllegalStateException} will be thrown 
	 * if "and" and "or" option flags are both either on or off at the same time.  If both search terms 
	 * are either {@code null} or empty {@code String}, full records will be returned
	 * 
	 * @param isAndSelected	a {@code boolean} indicating if the specified search terms should be "and" together
	 * @param isOrSelected  a {@code boolean} indicating if the specified search terms should be "or" together
	 * @param location 		a {@code String} containing a search term.  May be {@code null}
	 * @param name 			a {@code String} containing a search term.  May be {@code null}
	 * @return a {@code String[][]} containing the matching records or full records if both search are 
	 * 		   either {@code null} or empty {@code String}
	 * 
	 * @throws RemoteException thrown if any issue with operation on RMI protocol occurs
	 */
	public String[][] searchRecords(boolean isAndSelected,
			boolean isOrSelected, String location, String name) {
		try {
			return this.service.searchRecords(isAndSelected, isOrSelected, location, name);
		} catch (RemoteException e) {
			logger.throwing(BusinessServiceAdaptor.class.getName(), "searchRecords", e);
		}
		return null;
	}

	/**
	 * Get field names
	 * 
	 * @return	a {@code String[]} containing the field names
	 */
	public String[] getFieldNames() {
		try {
			return this.service.getFieldNames();	
		} catch (RemoteException e) {
			logger.throwing(BusinessServiceAdaptor.class.getName(), "getFieldNames", e);
		}
		return null;
	}
}

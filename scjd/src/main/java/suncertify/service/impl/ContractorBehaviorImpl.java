package suncertify.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

import suncertify.DatabaseConfig;
import suncertify.Mode;
import suncertify.db.DBAccess;
import suncertify.db.Data;
import suncertify.db.DataFileHandler;
import suncertify.db.RecordNotFoundException;
import suncertify.gui.GuiController;
import suncertify.service.ContractorBehavior;


/**
 * A {@code ContractorBehaviorImpl} provides business logic implementation to 
 * {@code BusinessService} implementing classes
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class ContractorBehaviorImpl implements ContractorBehavior {

	/**
	 * A {@code Logger} that logs useful info.
	 */
	static private Logger logger = Logger.getLogger(ContractorBehaviorImpl.class.getName());

	/**
	 * A {@code DBAccess} that provides dao level service
	 */
	private DBAccess data;

	/**
	 * A {@code DatabaseConfig} provides settings configured from previous session.  
	 */
	private DatabaseConfig dbCfg;

	/**
	 * A default constructor.  A {@code IllegalStateException} will be thrown if specified mode isn't 
	 * expected or database file path cannot be retrieved
	 * 
	 * @param mode	a {@code Mode} that indicates the current running mode
	 * @throws IOException	when any issue with accessing database file occurs
	 */
	public ContractorBehaviorImpl(Mode mode) throws IOException {
		if (Mode.CLIENT == mode) {
			throw new IllegalStateException(String.format("%s shouldn't initiate server side service!", Mode.CLIENT));
		}

		this.dbCfg = new DatabaseConfig();
		Properties properites = this.dbCfg.load();

		Object dbFilePath = null;
		switch (mode) {
			case SERVER: {
				dbFilePath = properites.get(DatabaseConfig.PRPTY_NAME_MODE_SERVER_DB_FILE_PATH);
				break;
			}
			case ALONE: {
				dbFilePath = properites.get(DatabaseConfig.PRPTY_NAME_MODE_STANDALONE_DB_FILE_PATH);
				break;
			}
			default: {
				throw new IllegalStateException(String.format("%s isn't supported!", mode));
			}
		}

		if (dbFilePath == null) {
			throw new IllegalStateException(
					String.format("dbFilePath must not be null!. Make sure db file path for mode[%s] is persisted in suncertify.properties", mode));
		}
		logger.finer(String.format("loading [%s] db file for mode[%s] ..", dbFilePath, mode ));
		this.data = new Data(new File((String) dbFilePath));
	}


	/*
	 * (non-Javadoc)
	 * @see suncertify.service.ContractorBehavior#updateRecord(java.lang.String[])
	 */
	public void updateRecord(String[] updatedRecord) {
		String[] searchTerms = new String[] {updatedRecord[0], updatedRecord[1], updatedRecord[2], null, null, null};
		long[] recNums= this.data.findByCriteria(searchTerms);
		
		if (recNums.length != 1) {
			throw new IllegalStateException(String.format("no or more than 1 rec. num is found! result[%s], search terms[%s]", 
					Arrays.toString(recNums), Arrays.deepToString(searchTerms)));
		}
		
		long recNum = recNums[0];
		long lockCookie;
		try {
			lockCookie = this.data.lockRecord(recNum);
			this.data.updateRecord(recNum, updatedRecord, lockCookie);
			this.data.unlock(recNum, lockCookie);
		} catch (RecordNotFoundException e1) {
			throw new IllegalStateException(e1);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see suncertify.service.ContractorBehavior#searchRecords(boolean, boolean, java.lang.String, java.lang.String)
	 */
	public String[][] searchRecords(boolean isAndSelected,
			boolean isOrSelected, String location, String name) {
		if ((isAndSelected && isOrSelected) 
				|| (! isAndSelected && ! isOrSelected)) {
			IllegalStateException thrown = new IllegalStateException("Or/And radio buttons cannot be in select/de-selected " +
					"state at the same time!");
			logger.throwing(GuiController.class.getName(), "actionPerformed", 
					thrown);

			throw thrown;
		}

		//collect filter data for tablemodel
		String[][] filteredData = new String[][]{};

		long[] recNums = null;
		if ((location != null && location.trim().length() > 0) || (name != null && name.trim().length() > 0)) {
			
			//search rec. num. based on the search terms
			if (isAndSelected) {
				recNums = this.data.findByCriteria(new String[] {name, location, null, null, null, null});
			}
			if (isOrSelected) {
				Set<Long> foundData = new TreeSet<Long>();
				for (long recNum : this.data.findByCriteria(new String[] {name, null, null, null, null, null})) {
					foundData.add(recNum);
				}
				for (long recNum : this.data.findByCriteria(new String[] {null, location, null, null, null, null})) {
					foundData.add(recNum);
				}
				recNums = new long[foundData.size()];
				Iterator<Long> foundDataItereator = foundData.iterator();
				int i = 0;
				while (foundDataItereator.hasNext()) {
					recNums[i] = foundDataItereator.next();
					i ++;
				}
			}	
		} else {
			//default search if no search term is given
			recNums = this.data.findByCriteria(new String[] {null, null, null, null, null, null});
		}

		//collect record data of matching record numbers
		for (int i = 0 ; i < recNums.length ; i ++) {
			String[] data = null;
			try {
				data = this.data.readRecord(recNums[i]);
			} catch (RecordNotFoundException e1) {
				logger.throwing(GuiController.class.getName(), "actionPerformed", e1);
				continue;
			}

			if (filteredData.length == 0) {
				filteredData = new String[recNums.length][data.length];	
			}
			filteredData[i] = data;
		}
		return filteredData;
	}

	/*
	 * (non-Javadoc)
	 * @see suncertify.service.ContractorBehavior#getFieldNames()
	 */
	public String[] getFieldNames() {
		DataFileHandler dataAccess = Data.getDataFileHander();
		
		List<String> fields = new ArrayList<String>();
		for (Map<String, Short> field : dataAccess.getFields()) {
			fields.add(field.keySet().iterator().next());
		}
		return fields.toArray(new String[]{});
	}
}

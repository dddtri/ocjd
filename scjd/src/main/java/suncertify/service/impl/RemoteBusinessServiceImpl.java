package suncertify.service.impl;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Logger;

import suncertify.Mode;
import suncertify.service.ContractorBehavior;
import suncertify.service.RemoteBusinessService;

/**
 * A {@code RemoteBusinessServiceImpl} provides service calls to client in client mode.
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class RemoteBusinessServiceImpl extends UnicastRemoteObject implements RemoteBusinessService {

	/**
	 * a {@code long} for serialization integrity check
	 */
	private static final long serialVersionUID = -5420205075904088215L;

	/**
	 * A {@code Logger} that logs useful info.
	 */
	private static Logger logger = Logger.getLogger(RemoteBusinessServiceImpl.class.getName());

	/**
	 * A {@code String} that contains the rmi bind name
	 */
	public static final String RMI_BIND_NAME = "RemoteBusinessService";

	/**
	 * A {@code ContractorBehavior} interface that provides business logic methods
	 */
	private ContractorBehavior contractorBehavior;

	/**
	 * A default constructor
	 * 
	 * @throws IOException	when any issue with accessing database file occurs 
	 * @throws RemoteException	when any issue with RMI protocol occurs
	 */
	public RemoteBusinessServiceImpl() throws IOException, RemoteException {
		this.contractorBehavior = new ContractorBehaviorImpl(Mode.SERVER);
	}


	/**
	 * Starts RMI server according to specified setting
	 * @param ipAddress	a {@code String} that specifies server ip address.  Must not be {@code null}
	 * @param port		a {@code int} that specifies the server port.  Must not be negative. 
	 * @throws IOException	when any issue with accessing database file occurs 
	 * @throws RemoteException	when any issue with RMI protocol occurs
	 */
	public static void startService(String ipAddress, int port) throws RemoteException, IOException {
		if (ipAddress == null) {
			throw new NullPointerException("ipAddress cannot be null!");
		}
		if (port < 0) {
			throw new IllegalArgumentException("port must not be negative");
		}
		logger.entering(RemoteBusinessServiceImpl.class.getName(), "startService", new Object[] {ipAddress, port});
		RemoteBusinessService service = new RemoteBusinessServiceImpl();

		Registry reg = LocateRegistry.getRegistry(ipAddress, port);

		reg.rebind("RemoteBusinessService", service);
		logger.exiting(RemoteBusinessServiceImpl.class.getName(), "startService", service);
	}

	/*
	 * (non-Javadoc)
	 * @see suncertify.service.RemoteBusinessService#updateRecord(java.lang.String[])
	 */
	public void updateRecord(String[] updatedRecord) throws RemoteException {
		this.contractorBehavior.updateRecord(updatedRecord);
	}

	/*
	 * (non-Javadoc)
	 * @see suncertify.service.RemoteBusinessService#searchRecords(boolean, boolean, java.lang.String, java.lang.String)
	 */
	public String[][] searchRecords(boolean isAndSelected,
			boolean isOrSelected, String location, String name) throws RemoteException {
		return this.contractorBehavior.searchRecords(isAndSelected, isOrSelected, location, name);
	}

	/*
	 * (non-Javadoc)
	 * @see suncertify.service.BusinessService#getFieldNames()
	 */
	public String[] getFieldNames() throws RemoteException {
		return this.contractorBehavior.getFieldNames();
	}
}

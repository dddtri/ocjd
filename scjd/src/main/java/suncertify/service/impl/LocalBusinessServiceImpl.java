package suncertify.service.impl;

import java.io.IOException;

import suncertify.Mode;
import suncertify.service.ContractorBehavior;
import suncertify.service.LocalBusinessService;

/**
 * A {@code LocalBusinessServiceImpl} provides service calls to client in alone mode
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class LocalBusinessServiceImpl implements LocalBusinessService {

	/**
	 * A {@code ConstructorBehavior} that provides record operation implementation
	 */
	private ContractorBehavior contractorBehavior; 
	
	/**
	 * A default constructor
	 * @throws IOException thrown if any issue occurs with operating on data base file
	 */
	public LocalBusinessServiceImpl() throws IOException {
		this.contractorBehavior = new ContractorBehaviorImpl(Mode.ALONE);
	}
	
	/*
	 * (non-Javadoc)
	 * @see suncertify.service.LocalBusinessService#updateRecord(java.lang.String[])
	 */
	public void updateRecord(String[] updatedRecord) {
		this.contractorBehavior.updateRecord(updatedRecord);
	}
	
	/*
	 * (non-Javadoc)
	 * @see suncertify.service.LocalBusinessService#searchRecords(boolean, boolean, java.lang.String, java.lang.String)
	 */
	public String[][] searchRecords(boolean isAndSelected,
			boolean isOrSelected, String location, String name) {
		return this.contractorBehavior.searchRecords(isAndSelected, isOrSelected, location, name);
	}

	/*
	 * (non-Javadoc)
	 * @see suncertify.service.BusinessService#getFieldNames()
	 */
	public String[] getFieldNames() {
		return this.contractorBehavior.getFieldNames();
	}
}

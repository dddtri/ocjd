package suncertify.service;

import java.rmi.RemoteException;

/**
 * A {@code ContractorBehavior} interface provides business logic implementation to {@code BusinessService} 
 * implementing classes
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public interface ContractorBehavior {

	/**
	 * Update record based on foreign key of provided record
	 * 
	 * @param updatedRecord a {@code String[]} that contains updated version of record to 
	 * 						be saved into the db file
	 *  
	 */
	public void updateRecord(String[] updatedRecord);
	
	/**
	 * Search records based on specified parameters. {@code IllegalStateException} will be thrown 
	 * if "and" and "or" option flags are both either on or off at the same time.  If both search terms 
	 * are either {@code null} or empty {@code String}, full records will be returned
	 * 
	 * @param isAndSelected	a {@code boolean} indicating if the specified search terms should be "and" together
	 * @param isOrSelected  a {@code boolean} indicating if the specified search terms should be "or" together
	 * @param location 		a {@code String} containing a search term.  May be {@code null}
	 * @param name 			a {@code String} containing a search term.  May be {@code null}
	 * @return a {@code String[][]} containing the matching records or full records if both search are 
	 * 		   either {@code null} or empty {@code String}
	 * 
	 * @throws RemoteException thrown if any issue with operation on RMI protocol occurs
	 */
	public String[][] searchRecords(boolean isAndSelected,
			boolean isOrSelected, String location, String name);

	/**
	 * Get field names
	 * 
	 * @return	a {@code String[]} containing the field names
	 */
	public String[] getFieldNames();
}

package suncertify.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * A {@code RemoteBusinessService} interface provides service calls to the remote 
 * client through RMI protocol.  
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public interface RemoteBusinessService extends BusinessService, Remote {

	/*
	 * (non-Javadoc)
	 * @see suncertify.service.BusinessService#updateRecord(java.lang.String[])
	 */
	public void updateRecord(String[] updatedRecord) throws RemoteException;
	
	/*
	 * (non-Javadoc)
	 * @see suncertify.service.BusinessService#searchRecords(boolean, boolean, java.lang.String, java.lang.String)
	 */
	public String[][] searchRecords(boolean isAndSelected,
			boolean isOrSelected, String location, String name) throws RemoteException;
	
	/*
	 * (non-Javadoc)
	 * @see suncertify.service.BusinessService#getFieldNames()
	 */
	public String[] getFieldNames() throws RemoteException;
}

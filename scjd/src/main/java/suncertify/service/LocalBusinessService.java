package suncertify.service;


/**
 * A {@code LocalBusinessService} interface provides service calls to the 
 * client that lives in the same JVM.  
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public interface LocalBusinessService extends BusinessService {

	/**
	 * Update record based on foreign key of provided record
	 * 
	 * @param updatedRecord a {@code String[]} that contains updated version of record to 
	 * 						be saved into the db file
	 */
	public void updateRecord(String[] updatedRecord);
	
	/**
	 * Search records based on specified parameters. {@code IllegalStateException} will be thrown 
	 * if "and" and "or" option flags are both either on or off at the same time.
	 * 
	 * @param isAndSelected	a {@code boolean} indicating if the specified search terms should be "and" together
	 * @param isOrSelected  a {@code boolean} indicating if the specified search terms should be "or" together
	 * @param location 		a {@code String} containing a search term.  May be {@code null}
	 * @param name 			a {@code String} containing a search term.  May be {@code null}
	 * @return a {@code String[][]} containing the matching records
	 */
	public String[][] searchRecords(boolean isAndSelected,
			boolean isOrSelected, String location, String name);
	
	/**
	 * Get field names
	 * 
	 * @return	a {@code String[]} containing the field names
	 * 
	 */
	public String[] getFieldNames();
}

package suncertify;

/**
 * A {@code Mode} for referencing what mode the user want a the given application 
 * to be running in   
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public enum Mode {

	/**
	 * Maps to alone mode specfied by user
	 */
	ALONE,
	
	/**
	 * Maps to server mode specifiedy by user
	 */
	SERVER,
	
	/**
	 * Maps to client mode specified by user
	 */
	CLIENT
}

package suncertify.db;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A non-thread safe {@code DataFileHandler} that handles the read/write in file level 
 * according to the database schema.
 * 
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class DataFileHandler {
	
	private static final String ENCODING = "US-ASCII";

	/**
	 * A {@code RandomAccessFile} that does file read/write operation
	 */
	private RandomAccessFile fileAccessor;

	/**
	 * A {@code int} represent magic cookie value (one of the db file header value)
	 */
	private int magicCookieVal;


	/**
	 * A {@code int} represent total overall length of each record in bytes (one of the db file header value)
	 */
	private int totalOverallLengthInBytesOfEachRecord;

	/**
	 * one of the database file header
	 */
	private long totalLengthInBytesOfFile;

	/**
	 * one of the database file header
	 */
	private short numberOfFieldsInEachRecord;

	/**
	 * A {@code List} containing fields.  Each field consist of field name and corresponding value size in {@code byte}
	 * in {@code Map}
	 */
	private List<Map<String, Short>> fields;

	/**
	 * A {@code List} containing field values of all records 
	 */
	private List<List<String>> records;

	/**
	 * A database {@code File}
	 */
	private File file;
	
	/**
	 * A singular {@code DataFileHandler} that parses the database file
	 */
	private static DataFileHandler dataAccess;

	/**
	 * A constructor that will pre-load specified database file into memory.  
	 * This is for testing purpose.  Production code should use the static 
	 * createOrGet method to ensure singularity of this class
	 * 
	 * @param file	a {@code File} that is the database file
	 * @throws IOException if any IO operation to database file occurs
	 */
	public DataFileHandler(File file) throws IOException {
		this.file = file;
		this.load();
	}

	/**
	 * Load db file into memory.  Crucial data in memory are fields and records
	 * 
	 * @throws IOException if any IO operation to database file occurs
	 * 
	 * @see #fields
	 * @see #records
	 */
	public void load() throws IOException {
		this.fileAccessor = new RandomAccessFile(file, "r");

		//reads start of file section
		this.magicCookieVal = this.fileAccessor.readInt();
		this.totalOverallLengthInBytesOfEachRecord = this.fileAccessor.readInt();
		this.numberOfFieldsInEachRecord = this.fileAccessor.readShort();

		//reads schema description section
		this.fields = new ArrayList<Map<String, Short>>();
		for (short recNum = 0 ; recNum < this.numberOfFieldsInEachRecord ; recNum++) {
			this.fields.add(new HashMap<String, Short>());

			short lengthInBytesOfFieldName = this.fileAccessor.readShort();
			byte[] fieldNameInBytes = new byte[lengthInBytesOfFieldName];
			this.fileAccessor.readFully(fieldNameInBytes);

			short fieldLengthInBytes = this.fileAccessor.readShort();
			fields.get(recNum).put(new String(fieldNameInBytes), fieldLengthInBytes);
		}

		//read data section
		this.totalLengthInBytesOfFile = this.fileAccessor.length();
		this.records = new ArrayList<List<String>>();
		while (this.fileAccessor.getFilePointer() < this.totalLengthInBytesOfFile) {
			this.records.add(new ArrayList<String>());
			int idx = this.records.size() - 1;

			int fieldNo = 0;
			try {
				byte deleteFlag = this.fileAccessor.readByte();
				this.records.get(idx).add(deleteFlag == 0 ? "false" : "true");
	
				for (Map<String, Short> field : this.fields) {
					fieldNo ++;
					short lengthInBytesOfField = field.values().iterator().next();
					byte[] fieldInBytes = new byte[lengthInBytesOfField];
					this.fileAccessor.readFully(fieldInBytes);
	
					this.records.get(idx).add(new String(fieldInBytes));
				}
			} catch (IOException e) {
				new IllegalStateException(String.format("Parse error occur for rec. no[%s] of field[%s]", idx, fieldNo));
			}
		}

		this.fileAccessor.close();
	}

	
	/**
	 * Update record of specified rec. number with the specified data value  {@code IllegalStateException} 
	 * occurs if any issue with write operation occurs
	 * 
	 * @param recNo		a rec. number in {@code long} type
	 * @param data		a {@code String[]} containing the record data to be saved into the database
	 * 
	 * @throws RecordNotFoundException	if the specified rec. number cannot be found in the database
	 */
	public void updateRecord(long recNo, String[] data)
			throws RecordNotFoundException {
		assertRecordExists(recNo);
		if (data.length != this.numberOfFieldsInEachRecord) {
			throw new IllegalArgumentException(String.format("data length should be [%s] insteand of [%s]!", this.numberOfFieldsInEachRecord, data.length));
		}

		List<String> record = new ArrayList<String>(Arrays.asList(data));
		record.add(0, "false");
		this.records.set((int) recNo, record);

		try {
			this.write();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}

	}

	/**
	 * Read record of specified rec. number. 
	 * 
	 * @param recNo	a {@code long} containing record number
	 * 
	 * @throws RecordNotFoundException	if the provided record number does not 
	 * 									exist in the database
	 */
	public String[] readRecord(long recNo) throws RecordNotFoundException {
		this.assertRecordExists(recNo);
		String[] rawRecord = this.records.get((int) recNo).toArray(new String[]{});
		return Arrays.copyOfRange(rawRecord, 1, rawRecord.length);
	}

	/**
	 * Delete record of specified rec. number.  {@code IllegalStateException} 
	 * occurs if any issue with write operation occurs 
	 * 
	 * @param recNo	a {@code long} containing record number
	 * 
	 * @throws RecordNotFoundException	if the provided record number does not 
	 * 									exist in the database
	 */
	public void deleteRecord(long recNo) throws RecordNotFoundException {
		this.assertRecordExists(recNo);
		List<String> rawRecord = this.records.get((int) recNo);
		rawRecord.set(0, "true");

		try {
			this.write();
		} catch (IOException e) {
			throw new IllegalStateException(e.getLocalizedMessage());
		}
	}


	/**
	 *  Finds records by provided criteria.
	 *  
	 *  <p>
	 *  <h1>matching rule</h1>
	 *   Field n in the database file is described by criteria[n]. 
	 *  A null value in criteria[n] matches any field value. A non-null  value in 
	 *  criteria[n] matches any field value that begins with criteria[n]. (For example, 
	 *  "Fred" matches "Fred" or "Freddy".)
	 *  </p>
	 *  
	 *  @param	criteria a {@code String[]} containing criteria for record search.  The 
	 *  				 provided array length must be in correct length
	 *  
	 *  @return a {@code long[]} containing record numbers of matching records
	 *  */
	public long[] findByCriteria(String[] criteria) {
		assertCriteriaIsValidFormat(criteria);
		Set<Long> matchedRecNo = new LinkedHashSet<Long>();
		records:
			for (int i = 0 ; i < this.records.size() ; i ++) {
				List<String> record = this.records.get(i);

				if ("true".equals(record.get(0))) {
					continue records;
				}
				for (int j = 1 ; j < record.size() ; j ++) {
					String expVal = criteria[j - 1];
					String actVal = record.get(j);
					if (expVal != null && ! actVal.startsWith(expVal)) {
						continue records;
					}

					if (j + 1 >= record.size()) {
						matchedRecNo.add((long) i); 
					}
				}
			}

		//convert to primite array
		long[] matchedRecNoInPrimitive = new long[matchedRecNo.size()];
		Iterator<Long> iterator = matchedRecNo.iterator();
		int i = 0;
		while (iterator.hasNext()) {
			matchedRecNoInPrimitive[i] = iterator.next();
			i ++;
		}

		return matchedRecNoInPrimitive;
	}
	

	/**
	 * Creates a record of specified data
	 * 
	 * @param data	a {@code String[]} containing full data to a record. Name, location, 
	 * 				specialties fields in the first 3 indices must not be {@code null} or empty
	 * 
	 * @return		a {@code long} indicating the record no. to the newly created record
	 * 
	 * @throws DuplicateKeyException if the record to be created key-duplicates to any record in the
	 * 								 database
	 */
	public long createRecord(String [] data) throws DuplicateKeyException {
		this.assertCriteriaIsValidFormat(data);
		String name = data[0];
		if (name == null || name.length() <= 0) {
			throw new IllegalArgumentException("name cannot be null or empty!");
		}
		
		String location = data[1];
		if (location == null || location.length() <= 0) {
			throw new IllegalArgumentException("location cannot be null or empty!");
		}
		
		String specialities = data[2];
		if (specialities == null || specialities.length() <= 0) {
			throw new IllegalArgumentException("specialties cannot be null or empty!");
		}

		//check no record duplicate "name", "location", "specialities" in db
		String[] criteria = new String[] {name, location, specialities, null, null, null};
		if (this.findByCriteria(criteria).length > 0) {
			throw new DuplicateKeyException(
			String.format("record of name[%s], location[%s], specialities[%s] already " +
			"exists in the db", name, location, specialities));
		}
		
		ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(data));
		arrayList.add(0, "false");
		this.records.add(arrayList);
		
		try {
			this.write();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		
		return this.records.size() - 1;
	}


	/**
	 * Overwrites complete data in memory into database file
	 * 
	 * @throws IOException if any IO operation to database file occurs
	 */
	public void write() throws IOException {
		this.fileAccessor = new RandomAccessFile(file, "rw");

		//reads start of file section
		this.fileAccessor.writeInt(this.magicCookieVal);
		this.fileAccessor.writeInt(this.totalOverallLengthInBytesOfEachRecord);
		this.fileAccessor.writeShort(this.numberOfFieldsInEachRecord);

		//writes schema description section
		for (short i = 0 ; i < this.numberOfFieldsInEachRecord ; i++) {
			byte[] fieldNameInBytes = this.fields.get(i).keySet().iterator().next().getBytes(ENCODING);
			short lengthInBytesOfFieldName = (short) fieldNameInBytes.length;
			short lengthInBytesOfField = this.fields.get(i).values().iterator().next();
			this.fileAccessor.writeShort(lengthInBytesOfFieldName);
			this.fileAccessor.write(fieldNameInBytes);
			this.fileAccessor.writeShort(lengthInBytesOfField);
		}

		//write data section
		Iterator<List<String>> recordsIterator = this.records.iterator();
		while (recordsIterator.hasNext()) {
			Iterator<String> recordIt = recordsIterator.next().iterator();
			this.fileAccessor.writeByte(("false".equals(recordIt.next()) ? 0 : 1));

			while (recordIt.hasNext()) {
				String colVal = recordIt.next();
				if (colVal == null) {
					colVal = "";
				}
				this.fileAccessor.write(colVal.getBytes(ENCODING));
			}
		}

		this.fileAccessor.close();
	}
	
	/**
	 * Gets fields. Each field consist of field name and corresponding value size in {@code byte}
	 * in {@code Map}
	 * @return 	a {@code List} of {@code Map} containing fields, each consist of field name and corresponding value 
	 * 			size in {@code byte}
	 */
	public List<Map<String, Short>> getFields() {
		return fields;
	}

	/**
	 * Gets all data records
	 * 
	 * @return a {@code List} containing all data records
	 */
	public List<List<String>> getRecords() {
		return records;
	}


	/**
	 * Dump data to console for testing purpose
	 * 
	 * @throws IOException if any IO operation to database file occurs
	 */
	public void printConsole() throws IOException {
		StringBuilder sb = new StringBuilder();
		sb.append("magic cookie value: " + this.magicCookieVal + "\n");
		sb.append("numberOfFieldsInEachRecord" + this.numberOfFieldsInEachRecord + "\n");
		sb.append("totalLengthInBytesOfFile" + this.totalLengthInBytesOfFile + "\n");
		sb.append("totalOverallLengthInBytesOfEachRecord" + this.totalOverallLengthInBytesOfEachRecord + "\n");
		Iterator<Map<String, Short>> fieldsIterator = this.fields.iterator();
		while (fieldsIterator.hasNext()) {
			Map<String, Short> fieldInfo = fieldsIterator.next();
			String fieldName = fieldInfo.keySet().iterator().next();
			Short size = fieldInfo.values().iterator().next();
			sb.append(String.format("%s[%s],", fieldName, size));
		}
		sb.append("\n");

		Iterator<List<String>> recordsIterator = this.records.iterator();
		while (recordsIterator.hasNext()) {
			for (String val : recordsIterator.next()) {
				sb.append(val + ",");
			}
			sb.append("\n");
		}

		System.out.println(sb.toString());
	}
	
	/**
	 * Asserts that records is loaded up into memory.  Throws 
	 * {@code IllegalStateException} if no records can be found in memory  
	 */
	private void assertDataInMemoryIsReady() {
		if (this.records.size() <= 0) {
			throw new IllegalStateException("records hasnt been loaded into memory, or data file contains empty record!");
		}
	}
	
	/**
	 * Assert record of specified rec. number exists in the database
	 * 
	 * @param recNo	a rec. number in {@code long} type
	 * 
	 * @throws RecordNotFoundException
	 */
	private void assertRecordExists(long recNo) throws RecordNotFoundException {
		this.assertDataInMemoryIsReady();
		if (this.records.size() < recNo) {
			throw new RecordNotFoundException(
					String.format("rec. no.[%s] doesn't exist in db file!", recNo));
		}
		if ("true".equals(this.records.get((int) recNo).get(0))) {
			throw new RecordNotFoundException(
					String.format("rec. no.[%s] is marked as deleted in db file!", recNo));
		}
	}

	/**
	 * Asserts criteria is in valid format.  Throws {@code IllegalArgumentException} if provided criteria array isn't in 
	 * valid length
	 * 
	 * @param criteria	a {@code String[]} containing criteria for record search
	 */
	private void assertCriteriaIsValidFormat(String[] criteria) {
		this.assertDataInMemoryIsReady();
		int expCiteriaLength = this.records.get(0).size() - 1;
		if (expCiteriaLength != criteria.length) {
			throw new IllegalArgumentException(String.format("provided criteria must be in length of %s", expCiteriaLength));
		}
	}
	
	/**
	 * instantiates or get a {@code DataFileHandler}
	 * 
	 * @param file	a {@code File} that targets on the database file
	 * 
	 * @return	a {@code DataFileHandler}
	 * 
	 * @throws IOException if any IO operation to database file occurs
	 */
	public static DataFileHandler getInstance(File file) throws IOException {
		if (dataAccess == null) {
			synchronized(Data.class) {
				if (dataAccess == null) {
					dataAccess = new DataFileHandler(file);
				}
			}
		}
		
		return dataAccess;
	}
}

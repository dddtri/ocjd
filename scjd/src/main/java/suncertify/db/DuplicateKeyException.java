package suncertify.db;

/**
 * A {@code DuplicateKeyException} that is thrown when a record intended to 
 * insert has a key duplication with any record that exists in the database
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class DuplicateKeyException extends Exception {

	/**
	 * A serial version uid 
	 */
	private static final long serialVersionUID = -6689165809485807888L;


	/**
	 * A default constructor with no specified input
	 */
	public DuplicateKeyException() {
		super();
	}
	
	
	/**
	 * A constructor that accepts exception description
	 * 
	 * @param desc	a {@code String} that contains exception description
	 */
	public DuplicateKeyException(String desc) {
		super(desc);
	}
	
}

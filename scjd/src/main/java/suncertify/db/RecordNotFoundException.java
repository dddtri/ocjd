package suncertify.db;

/**
 * A {@code RecordNotFoundException} that is thrown when a specified record 
 * does not exist or is marked as deleted in the database file
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class RecordNotFoundException extends Exception {

	/**
	 * A serial version uid 
	 */
	private static final long serialVersionUID = 9172845648588845215L;

	/**
	 * A default constructor with no specified input
	 */
	public RecordNotFoundException() {
		super();
	}
	
	
	/**
	 * A constructor that accepts exception description
	 * 
	 * @param desc	a {@code String} that contains exception description
	 */
	public RecordNotFoundException(String desc) {
		super(desc);
	}
	
}

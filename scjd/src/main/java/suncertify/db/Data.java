package suncertify.db;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;

/**
 * A thread-safe {@code Data} class that provides dao level service to the 
 * {@code ConstructorBehavior} implementing classes.
 * 
 * @author Daniel (ShenYu) Shih
 *
 */
public class Data implements DBAccess {

	/**
	 * A {@code Logger} that logs useful info.
	 */
	private static Logger logger = Logger.getLogger(Data.class.getName());

	/**
	 * A {@code DataFileHandler} that parses the database file
	 */
	private static DataFileHandler dataAccess;

	/**
	 * a cookie {@code Map} that contains record no. as key and associated cookie as value
	 */
	private static Map<Long, Long> lockCookieMap = new HashMap<Long, Long>();

	/**
	 * a {@code ReentrantReadWriteLock} that manages read/write lock of database file
	 */
	private static ReentrantReadWriteLock dbLock = new ReentrantReadWriteLock();

	/**
	 * A constructor
	 * 
	 * @param file	a {@code file} that is the database file
	 * @throws IOException if any IO operation to database file occurs
	 */
	public Data(File file) throws IOException {
		dataAccess = DataFileHandler.getInstance(file);
		logger.exiting(Data.class.getName(), "Data", dataAccess);
	}

	/**
	 * Reads a record from the file. 
	 * 
	 * @param recNo a {@code long} containing record number
	 * @return an array where each element is a record value.
	 */
	public String[] readRecord(long recNo) throws RecordNotFoundException {
		logger.entering("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "readRecord", recNo);
		dbLock.readLock().lock();
		String[] record = null;
		try {
			record = dataAccess.readRecord(recNo);	
		} finally {
			dbLock.readLock().unlock();
		}
		logger.exiting("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "readRecord", Arrays.deepToString(record));
		return record;
	}

	/**
	 * Modifies the fields of a record. The new value for field n appears in data[n].
	 * 
	 * @param recNo		a {@code long} that specifies the row index of the records list to be 
	 * 					updated
	 * @param data		a {@code String[]} that contains updated version of record to 
	 * 						be saved into the db file
	 * @param lockCookie a {@code long} containing the lock cookie number
	 * @throws SecurityException  if the record is locked with a cookie other than lockCookie.
	 * @throws RecordNotFoundException if the record cannot be found
	 */
	public void updateRecord(long recNo, String[] data, long lockCookie)
			throws RecordNotFoundException, SecurityException {
		logger.entering("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "updateRecord", new Object[]{recNo, Arrays.deepToString(data), lockCookie});

		assertCookieIsAcquiredByRecNum(recNo, lockCookie);

		dbLock.writeLock().lock();
		try {
			dataAccess.updateRecord(recNo, data);
		} finally {
			dbLock.writeLock().unlock();
		}
		logger.exiting("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "updateRecord", Arrays.deepToString(data));
	}


	/**
	 * Deletes a record, making the record number and associated disk storage available for reuse.
	 * 
	 * @param recNo		a {@code long} that specifies the associated record to be deleted
	 * @param lockCookie a {@code long} containing the lock cookie number
	 * @throws SecurityException  if the record is locked with a cookie other than lockCookie.
	 * @throws RecordNotFoundException if the record cannot be found
	 */
	public void deleteRecord(long recNo, long lockCookie)
			throws RecordNotFoundException, SecurityException {
		logger.entering("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "deleteRecord", new Object[]{recNo, lockCookie});

		assertCookieIsAcquiredByRecNum(recNo, lockCookie);

		dbLock.writeLock().lock();
		try {
			dataAccess.deleteRecord(recNo);
		} finally {
			dbLock.writeLock().unlock();
		}
		logger.exiting("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "deleteRecord");
	}

	/**
	 * Returns an array of record numbers that match the specified criteria. Field n in the database file 
	 * is described by criteria[n]. A null value in criteria[n] matches any field value. A non-null  value 
	 * in criteria[n] matches any field value that begins with criteria[n]. (For example, "Fred" matches "Fred" or 
	 * "Freddy".)
	 * 
	 * @param	criteria a {@code String[]} containing criteria for record search.  The 
	 *  				 provided array length must be in correct length
	 * @return a {@code long[]} containing record numbers of matching records
	 */
	public long[] findByCriteria(String[] criteria) {
		logger.entering("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "findByCriteria", criteria);

		dbLock.readLock().lock();
		long[] recNums;
		try  {
			recNums = dataAccess.findByCriteria(criteria);
		} finally {
			dbLock.readLock().unlock();
		}

		logger.exiting("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "findByCriteria", Arrays.deepToString(new Object[]{recNums}));
		return recNums;
	}

	/**
	 * Creates a new record in the database (possibly reusing a deleted entry). Inserts the given data, and returns 
	 * the record number of the new record.
	 * 
	 * @param data	a {@code String[]} containing full data to a record. Name, location, 
	 * 				specialties fields in the first 3 indices must not be {@code null} or empty
	 * 
	 * @return		a {@code long} indicating the record no. to the newly created record
	 * 
	 * @throws DuplicateKeyException if the record to be created key-duplicates to any record in the
	 * 								 database
	 */
	public long createRecord(String[] data) throws DuplicateKeyException {
		logger.entering("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "createRecord", data);
		dbLock.writeLock().lock();
		long recNum;
		try {
			recNum = dataAccess.createRecord(data);			
		} finally {
			dbLock.writeLock().unlock();
		}
		logger.exiting("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "createRecord", recNum);
		return recNum;

	}

	/**
	 * Locks a record so that it can only be updated or deleted by this client. Returned value is a cookie that 
	 * must be used when the record is unlocked, updated, or deleted. If the specified record is already locked 
	 * by a different client, the current thread gives up the CPU and consumes no CPU cycles until the record is unlocked.
	 * 
	 * @param recNo	a {@code long} containing a record number
	 * @return	a {@code long} containing the corresponding lock cookie value
	 * @throws RecordNotFoundException if the record cannot be found
	 */
	public long lockRecord(long recNo) throws RecordNotFoundException {
		logger.entering("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "lockRecord", recNo);

		synchronized (lockCookieMap) {
			while (lockCookieMap.containsKey(recNo)) {
				try {
					lockCookieMap.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			this.readRecord(recNo);
			lockCookieMap.put(recNo, System.currentTimeMillis());
		}

		long lockCookie = lockCookieMap.get(recNo);
		logger.exiting("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "lockRecord", lockCookie);
		return lockCookie;
	}

	/**
	 * Releases the lock on a record. Cookie must be the cookie returned when the record was locked; 
	 * otherwise throws SecurityException.
	 * 
	 * @param recNo		a {@code long} that specifies the row index of the records list to be 
	 * 					updated
	 * @param cookie    a {@code long} containing the lock cookie number
	 * @throws SecurityException  if the record is locked with a cookie other than lockCookie.
	 */
	public void unlock(long recNo, long cookie) throws SecurityException {
		logger.entering("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "unlock", new Object[]{recNo, cookie});

		synchronized(lockCookieMap) {
			assertCookieIsAcquiredByRecNum(recNo, cookie);
			lockCookieMap.remove(recNo);
			lockCookieMap.notifyAll();
		}

		logger.exiting("[" + Thread.currentThread().getName() + "]" + Data.class.getName(), "unlock");
	}
	
	/**
	 * Asserts that the specified cookie of the specified record has been acquired.  
	 * {@code SecurityException} is thrown either when the cookie isn't in acquired state or 
	 * the acquired state doesn't belong to the target record number
	 *  
	 * @param recNo			a {@code long} containing the target record number
	 * @param lockCookie	a {@code long} containing the cookie number that should be 
	 * 						in acquired state 
	 */
	private void assertCookieIsAcquiredByRecNum(long recNo, long lockCookie) {
		if (! lockCookieMap.containsKey(recNo)) {
			throw new SecurityException(
					String.format("record[%s] needs to be locked before db operation!", 
							recNo, lockCookieMap.get(recNo), lockCookie));
		}

		if (lockCookieMap.containsKey(recNo) && lockCookieMap.get(recNo) != lockCookie) {
			throw new SecurityException(
					String.format("record[%s] is locked with cookie[%s] other than lockCookie[%s]", 
							recNo, lockCookieMap.get(recNo), lockCookie));
		}
	}

	/**
	 * Gets {@code DataFileHandler}.  {@code IllegalStateException} occurs if {@code DataFileHandler} 
	 * is {@code null}
	 * 
	 * @return	a {@code DataFileHandler}.  Won't be {@code null}
	 */
	public static DataFileHandler getDataFileHander() {
		if (dataAccess == null) {
			throw new IllegalStateException("dataAcces hasn't being created!");
		}
		return dataAccess;
	}
}
